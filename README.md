﻿一、简介

AS6802 Core实现了AS6802规范中工作在标准完整性模式下的同步主控器（SM），以及压缩主控器（CM）的功能逻辑，包括
时间同步服务、结团检测服务、状态机转移、PCF帧固化、PCF帧压缩等功能。芯准AS6802 Core的总体架构由
PCF固化模块（PCFPermntModule）、SM顶层模块（SMTopModule）、CM顶层模块（CMTopModule）、本地时钟选择模块（LocalClkPickModule）、输出PCF选择模块（OutPCFPickModule）以及PCF派发模块（PCFDispatchModule）组成。

二、目录结构
1、doc目录 该目录用于存放文档，包括芯准AS6802 Core概要设计方案；
2、src目录 该目录用于存放FPGA硬件源码，包含CM顶层模块Verilog源码、SM顶层模块Verilog源码、
、PCF固化模块Verilog源码、本地时钟选择模块Verilog源码、PCF派发模块Verilog源码。

