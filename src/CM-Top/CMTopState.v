/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: CMTopState.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module CMTopState(
    input  wire        SYS2CORE_clk,
    input  wire        SYS2CORE_rst_n,

    output wire [ 7:0] stable_cycle_ctr,
    output wire [ 5:0] current_top_state,
                       
    input  wire        CS2CTS_next_top_state_valid,
    input  wire [ 5:0] CS2CTS_next_top_state,
    input  wire [36:0] CS2CTS_bottom_state
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
reg [7:0] next_stable_cycle_ctr;
reg [7:0] curr_stable_cycle_ctr;

reg [5:0] reg_top_state;

localparam CM_INTEGRATE  = 6'b000001,
           CM_UNSYNC     = 6'b000010,
           CM_CA_ENABLED = 6'b000100,
           CM_WAIT_4_IN  = 6'b001000,
           CM_SYNC       = 6'b010000,
           CM_STABLE     = 6'b100000;
/*//////////////////////////////////////////////////////////
            同步稳定周期计数器管理StableCycleCtrMgmt
*///////////////////////////////////////////////////////////


assign stable_cycle_ctr = curr_stable_cycle_ctr;//稳定计数器没有立刻得到结果的需求，可以选择寄存器结果输出

always @* begin//modify by lxj 20200427
    if((CS2CTS_bottom_state[2+8+1] == 1'b1) || 
       (CS2CTS_bottom_state[2+8+2] == 1'b1) || 
       (CS2CTS_bottom_state[2+8+5] == 1'b1) || 
       (CS2CTS_bottom_state[2+0+1] == 1'b1) || 
       (CS2CTS_bottom_state[2+0+3] == 1'b1) || 
       (CS2CTS_bottom_state[2+0+4] == 1'b1)
       )begin//清零
        next_stable_cycle_ctr = 8'b0;
    end
    else if(
        (CS2CTS_bottom_state[2+8+3] == 1'b1) || 
        (CS2CTS_bottom_state[2+8+4] == 1'b1) || 
        (CS2CTS_bottom_state[2+0+2] == 1'b1)
        )begin//加1
        next_stable_cycle_ctr = curr_stable_cycle_ctr + 1'b1;
    end
    else begin
        next_stable_cycle_ctr = curr_stable_cycle_ctr;
    end
end

always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(SYS2CORE_rst_n == 1'b0) begin
        curr_stable_cycle_ctr <= 8'b0;
    end
    else begin
        curr_stable_cycle_ctr <= next_stable_cycle_ctr;
    end
end

/*//////////////////////////////////////////////////////////
            CM主状态管理TopStateMgmt
*///////////////////////////////////////////////////////////
assign current_top_state = (CS2CTS_next_top_state_valid == 1'b1) ? CS2CTS_next_top_state : reg_top_state;

always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(SYS2CORE_rst_n == 1'b0) begin
        reg_top_state <= CM_INTEGRATE;
    end
    else begin
        reg_top_state <= current_top_state;
    end
end
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP....
endmodule
/*
CMTopState CMTopState_inst(
    .SYS2CORE_clk(),
    .SYS2CORE_rst_n(),

    .stable_cycle_ctr(),
    .current_top_state(),

    .CS2CTS_next_top_state_valid(),
    .CS2CTS_next_top_state(),
    .CS2CTS_bottom_state()
);
*/