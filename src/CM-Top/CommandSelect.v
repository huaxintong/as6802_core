/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: CommandSelect.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module CommandSelect(
    input  wire         SYS2CORE_clk,
    
    input  wire [  7:0] stable_cycle_ctr,
    input  wire [  5:0] current_top_state,
    
    input  wire         G_pcf_des_wr,
    input  wire [ 90:0] G_pcf_des,
                  
    input  wire         CI2CS_cmd_valid,
    input  wire [  5:0] CI2CS_cmd_next_top_state,
    input  wire [  4:0] CI2CS_cmd_bottom_state,
                  
    input  wire         CCE2CS_cmd_valid,
    input  wire [  5:0] CCE2CS_cmd_next_top_state,
    input  wire [  3:0] CCE2CS_cmd_bottom_state,
                  
    input  wire         CU2CS_cmd_valid,
    input  wire [  5:0] CU2CS_cmd_next_top_state,
    input  wire [  5:0] CU2CS_cmd_bottom_state,
                  
    input  wire         CWI2CS_cmd_valid,
    input  wire [  5:0] CWI2CS_cmd_next_top_state,
    input  wire [  4:0] CWI2CS_cmd_bottom_state,
                  
    input  wire         SYN2CS_cmd_valid,
    input  wire [  5:0] SYN2CS_cmd_next_top_state,
    input  wire [  8:0] SYN2CS_cmd_bottom_state,
                  
    input  wire         STA2CS_cmd_valid,
    input  wire [  5:0] STA2CS_cmd_next_top_state,
    input  wire [  7:0] STA2CS_cmd_bottom_state,
    
    output reg          CS2CTS_next_top_state_valid,
    output reg  [  5:0] CS2CTS_next_top_state,
    output wire [ 36:0] CS2CTS_bottom_state,
    
    output wire [ 42:0] G_current_state,
    
    output wire [298:0] CORE2MON_cm_state,
    
    output reg          state_clr_trigger//状态机中的清零条件开关
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
localparam CM_INTEGRATE  = 6'b000001,
           CM_UNSYNC     = 6'b000010,
           CM_CA_ENABLED = 6'b000100,
           CM_WAIT_4_IN  = 6'b001000,
           CM_SYNC       = 6'b010000,
           CM_STABLE     = 6'b100000;

/*//////////////////////////////////////////////////////////
                        输出信号处理
*///////////////////////////////////////////////////////////
//同步状态机中的清零条件触发信号
always @(posedge SYS2CORE_clk) 
    state_clr_trigger <= ((G_current_state[42:37] == CM_INTEGRATE) && (G_current_state[2+32+1] == 1'b1)) |
                         ((G_current_state[42:37] == CM_UNSYNC) && (G_current_state[2+26+1] == 1'b1)) |
                         ((G_current_state[42:37] == CM_UNSYNC) && (G_current_state[2+26+2] == 1'b1)) |
                         ((G_current_state[42:37] == CM_CA_ENABLED) && (G_current_state[2+22+1] == 1'b1)) |
                         ((G_current_state[42:37] == CM_WAIT_4_IN) && (G_current_state[2+17+2] == 1'b1)) |
                         ((G_current_state[42:37] == CM_SYNC) && (G_current_state[2+8+1] == 1'b1)) |
                         ((G_current_state[42:37] == CM_SYNC) && (G_current_state[2+8+2] == 1'b1)) |
                         ((G_current_state[42:37] == CM_STABLE) && (G_current_state[2+0+1] == 1'b1)) |
                         ((G_current_state[42:37] == CM_STABLE) && (G_current_state[2+0+3] == 1'b1));
                         

assign CS2CTS_bottom_state[36:32] = CI2CS_cmd_bottom_state;
assign CS2CTS_bottom_state[31:26] = CU2CS_cmd_bottom_state;
assign CS2CTS_bottom_state[25:22] = CCE2CS_cmd_bottom_state;
assign CS2CTS_bottom_state[21:17] = CWI2CS_cmd_bottom_state;
assign CS2CTS_bottom_state[16: 8] = SYN2CS_cmd_bottom_state;
assign CS2CTS_bottom_state[ 7: 0] = STA2CS_cmd_bottom_state;


assign G_current_state [42:37] = current_top_state[5:0];
assign G_current_state [36: 0] = CS2CTS_bottom_state;

always @* begin
    if(CI2CS_cmd_valid == 1'b1) begin//CM_INTEGRATE
        CS2CTS_next_top_state_valid = 1'b1;
        CS2CTS_next_top_state       = CI2CS_cmd_next_top_state;
    end
    else if(CU2CS_cmd_valid == 1'b1) begin//CM_UNSYNC
        CS2CTS_next_top_state_valid = 1'b1;
        CS2CTS_next_top_state       = CU2CS_cmd_next_top_state;
    end
    else if(CCE2CS_cmd_valid == 1'b1) begin//CM_CA_ENABLED
        CS2CTS_next_top_state_valid = 1'b1;
        CS2CTS_next_top_state       = CCE2CS_cmd_next_top_state;
    end
    else if(CWI2CS_cmd_valid == 1'b1) begin//CM_WAIT_4_IN
        CS2CTS_next_top_state_valid = 1'b1;
        CS2CTS_next_top_state       = CWI2CS_cmd_next_top_state;
    end
    else if(SYN2CS_cmd_valid == 1'b1) begin//CM_SYNC
        CS2CTS_next_top_state_valid = 1'b1;
        CS2CTS_next_top_state       = SYN2CS_cmd_next_top_state;
    end
    else if(STA2CS_cmd_valid == 1'b1) begin//CM_STABLE
        CS2CTS_next_top_state_valid = 1'b1;
        CS2CTS_next_top_state       = STA2CS_cmd_next_top_state;
    end
    else begin
        CS2CTS_next_top_state_valid = 1'b0;
        CS2CTS_next_top_state       = 6'b0;
    end
end

assign CORE2MON_cm_state[298:0]={91'b0,
                                 stable_cycle_ctr[7:0],
                                 G_pcf_des_wr,
                                 G_pcf_des,
                                 1'b0,
                                 64'b0,
                                 current_top_state[5:0],
                                 CS2CTS_bottom_state[36:0]};
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP....
endmodule
/*
CommandSelect CommandSelect_inst(
    .SYS2CORE_clk(),

    .stable_cycle_ctr(),
    .current_top_state(),
    
    .G_pcf_des_wr(),
    .G_pcf_des(),

    .CI2CS_cmd_valid(),
    .CI2CS_cmd_next_top_state(),
    .CI2CS_cmd_bottom_state(),

    .CCE2CS_cmd_valid(),
    .CCE2CS_cmd_next_top_state(),
    .CCE2CS_cmd_bottom_state(),

    .CU2CS_cmd_valid(),
    .CU2CS_cmd_next_top_state(),
    .CU2CS_cmd_bottom_state(),

    .CWI2CS_cmd_valid(),
    .CWI2CS_cmd_next_top_state(),
    .CWI2CS_cmd_bottom_state(),

    .SYN2CS_cmd_valid(),
    .SYN2CS_cmd_next_top_state(),
    .SYN2CS_cmd_bottom_state(),

    .STA2CS_cmd_valid(),
    .STA2CS_cmd_next_top_state(),
    .STA2CS_cmd_bottom_state(),

    .CS2CTS_next_top_state_valid(),
    .CS2CTS_next_top_state(),
    .CS2CTS_bottom_state(),
    
    .G_current_state(),
    
    .CORE2MON_cm_state(),

    .state_clr_trigger()//状态机中的清零条件开关
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);
*/