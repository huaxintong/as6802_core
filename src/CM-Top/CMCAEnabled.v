/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: CMCAEnabled.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module CMCAEnabled(
    input  wire       SYS2CORE_clk,
    input  wire       SYS2CORE_rst_n,

    input  wire       GA2CCE_guard_state_cs,

    output wire       CCE2GA_guard_pcf_ready,

    input  wire       GA2CCE_guard_timeout,

    output reg        CCE2CS_cmd_valid,
    output reg  [5:0] CCE2CS_cmd_next_top_state,
    output wire [3:0] CCE2CS_cmd_bottom_state
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
reg [3:0] next_cce_state;
reg [3:0] curr_cce_state;

localparam CM_INTEGRATE  = 6'b000001,
           CM_UNSYNC     = 6'b000010,
           CM_CA_ENABLED = 6'b000100,
           CM_WAIT_4_IN  = 6'b001000,
           CM_SYNC       = 6'b010000,
           CM_STABLE     = 6'b100000;
           
localparam INIT_S     = 4'b0001,
           MATCH_S    = 4'b0010,
           COMPLETE_S = 4'b0100,
           C0_S       = 4'b1000;
/*//////////////////////////////////////////////////////////
                    状态机跳转处理
*///////////////////////////////////////////////////////////
assign CCE2CS_cmd_bottom_state = next_cce_state;

assign CCE2GA_guard_pcf_ready = (next_cce_state == MATCH_S);



always @* begin
    case(curr_cce_state)
        INIT_S: begin
            if(GA2CCE_guard_state_cs == 1'b1)
                next_cce_state = MATCH_S;
            else
                next_cce_state = INIT_S;
        end
        
        MATCH_S: begin
            if(GA2CCE_guard_timeout == 1'b1)
                next_cce_state = C0_S;
            else
                next_cce_state = MATCH_S;
        end
        
        C0_S: begin
            next_cce_state = COMPLETE_S;
        end
        
        COMPLETE_S: begin
            next_cce_state = INIT_S;
        end
        
        default: next_cce_state = INIT_S;
    endcase
end

//时序逻辑用于缓存当前状态
always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(SYS2CORE_rst_n == 1'b0) begin
        curr_cce_state <= INIT_S;
    end
    else begin
        curr_cce_state <= next_cce_state;
    end
end

//在该时序逻辑部分实现各状态的执行动作
always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(SYS2CORE_rst_n == 1'b0) begin
        CCE2CS_cmd_valid <= 1'b0;
        CCE2CS_cmd_next_top_state <= 6'b0;
    end
    else begin
        case(curr_cce_state)
            INIT_S: begin
                CCE2CS_cmd_valid <= 1'b0;
                CCE2CS_cmd_next_top_state <= 6'b0;
            end
            
            MATCH_S:begin
                CCE2CS_cmd_valid <= 1'b0;
                CCE2CS_cmd_next_top_state <= 6'b0;
            end
            
            C0_S:begin
                CCE2CS_cmd_valid <= 1'b1;
                CCE2CS_cmd_next_top_state <= CM_WAIT_4_IN;
            end
            
            COMPLETE_S:begin
                CCE2CS_cmd_valid <= 1'b0;
                CCE2CS_cmd_next_top_state <= 6'b0;
            end
            
            default: begin
                CCE2CS_cmd_valid <= 1'b0;
                CCE2CS_cmd_next_top_state <= 6'b0;
            end
        endcase
    end
end
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP....   
endmodule
/*
CMCAEnabled CMCAEnabled_inst(
    .SYS2CORE_clk(),
    .SYS2CORE_rst_n(),

    .GA2CCE_guard_state_cs(),

    .CCE2GA_guard_pcf_ready(),

    .GA2CCE_guard_timeout(),

    .CCE2CS_cmd_valid(),
    .CCE2CS_cmd_next_top_state(),
    .CCE2CS_cmd_bottom_state()
);
*/