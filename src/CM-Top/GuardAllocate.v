/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: GuardAllocate.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module GuardAllocate(
    input  wire [ 19:0] G_local_clk,
    input  wire [336:0] CONF2CORE_cm_static_conf,

    input  wire         TOC2SCT_time_out,

    input  wire         G_pcf_des_wr,
    input  wire [ 90:0] G_pcf_des,
    output wire         SCT2PD_pcf_des_ready,

    input  wire [  5:0] CD2SCT_clique_in,
    input  wire [  7:0] stable_cycle_ctr,
    input  wire [  5:0] current_top_state,

    output wire         GA2CI_guard_state_cs,
    output wire         GA2CI_guard_pcf_valid,
    output wire [  2:0] GA2CI_guard_pcf,
    input  wire         CI2GA_guard_pcf_ready,
    output wire         GA2CI_guard_timeout,

    output wire         GA2CCE_guard_state_cs,
    output wire         GA2CCE_guard_timeout,
    input  wire         CCE2GA_guard_pcf_ready,

    output wire         GA2CU_guard_state_cs,
    output wire         GA2CU_guard_pcf_valid,
    output wire [  3:0] GA2CU_guard_pcf,
    input  wire         CU2GA_guard_pcf_ready,

    output wire         GA2CWI_guard_state_cs,
    output wire         GA2CWI_guard_pcf_valid,
    output wire [  2:0] GA2CWI_guard_pcf,
    input  wire         CWI2GA_guard_pcf_ready,
    output wire         GA2CWI_guard_timeout,

    output wire         GA2SYN_guard_state_cs,
    output wire [ 19:0] GA2SYN_local_clk,
    input  wire         SYN2GA_guard_pcf_ready,
    output wire [  7:0] GA2SYN_guard_stable_cycle_ctr,
    output wire [ 68:0] GA2SYN_guard_conf,
    output wire [  2:0] GA2SYN_guard_clique_in,

    output wire         GA2STA_guard_state_cs,
    output wire [ 19:0] GA2STA_local_clk,
    input  wire         STA2GA_guard_pcf_ready,
    output wire [  7:0] GA2STA_guard_stable_cycle_ctr,
    output wire [ 67:0] GA2STA_guard_conf,
    output wire [  2:0] GA2STA_guard_clique_in
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 


/*//////////////////////////////////////////////////////////
            输出信号处理
*///////////////////////////////////////////////////////////
//CMIntegrate子状态模块
assign SCT2PD_pcf_des_ready = (GA2CI_guard_state_cs  & CI2GA_guard_pcf_ready)  | 
                              (GA2CCE_guard_state_cs & CCE2GA_guard_pcf_ready) | 
                              (GA2CU_guard_state_cs  & CU2GA_guard_pcf_ready)  | 
                              (GA2CWI_guard_state_cs & CWI2GA_guard_pcf_ready) |
                              (GA2SYN_guard_state_cs & SYN2GA_guard_pcf_ready) | 
                              (GA2STA_guard_state_cs & STA2GA_guard_pcf_ready);
                              
assign GA2CI_guard_state_cs = current_top_state[0];

assign GA2CI_guard_pcf_valid = G_pcf_des_wr;
assign GA2CI_guard_pcf[2:0]  = {G_pcf_des[38],G_pcf_des[17:16]};
assign GA2CI_guard_timeout   = TOC2SCT_time_out;

//CMUnsync子状态模块
assign GA2CU_guard_state_cs  = current_top_state[1];
assign GA2CU_guard_pcf_valid = G_pcf_des_wr;
assign GA2CU_guard_pcf[3:0]  = {G_pcf_des[40:39],G_pcf_des[17:16]};
//assign GA2CU_guard_timeout   = TOC2SCT_time_out;//delete by lxj 20200513

//CMCAEnabled子状态模块
assign GA2CCE_guard_state_cs = current_top_state[2];
assign GA2CCE_guard_timeout  = TOC2SCT_time_out;



//CMWait4IN子状态模块
assign GA2CWI_guard_state_cs  = current_top_state[3];
assign GA2CWI_guard_pcf_valid = G_pcf_des_wr;
assign GA2CWI_guard_pcf[2:0]  = {G_pcf_des[39],G_pcf_des[17:16]};
assign GA2CWI_guard_timeout   = TOC2SCT_time_out;

//CMSync子状态模块
assign GA2SYN_guard_state_cs              = current_top_state[4];
assign GA2SYN_local_clk                   = G_local_clk;
assign GA2SYN_guard_stable_cycle_ctr[7:0] = stable_cycle_ctr[7:0];
assign GA2SYN_guard_conf[68:0]            = {CONF2CORE_cm_static_conf[188], 
                                             CONF2CORE_cm_static_conf[187:180], 
                                             CONF2CORE_cm_static_conf[59:40],
                                             CONF2CORE_cm_static_conf[39:20],
                                             CONF2CORE_cm_static_conf[19: 0]};
assign GA2SYN_guard_clique_in[2:0]        = {CD2SCT_clique_in[4],CD2SCT_clique_in[1:0]};

//CMStable子状态模块
assign GA2STA_guard_state_cs              = current_top_state[5];
assign GA2STA_local_clk                   = G_local_clk;
assign GA2STA_guard_stable_cycle_ctr[7:0] = stable_cycle_ctr[7:0];
assign GA2STA_guard_conf[67:0]            = {CONF2CORE_cm_static_conf[179:172],
                                             CONF2CORE_cm_static_conf[59:40],
                                             CONF2CORE_cm_static_conf[39:20],
                                             CONF2CORE_cm_static_conf[19:0]};
assign GA2STA_guard_clique_in[2:0]        = {CD2SCT_clique_in[5],CD2SCT_clique_in[3:2]};
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP....
endmodule
/*
GuardAllocate GuardAllocate_inst(
    .G_local_clk(),
    .CONF2CORE_cm_static_conf(),
    
    .TOC2SCT_time_out(),

    .G_pcf_des_wr(),
    .G_pcf_des(),
    .SCT2PD_pcf_des_ready(),
    
    .CD2SCT_clique_in(),
    .stable_cycle_ctr(),
    .current_top_state(),

    .GA2CI_guard_state_cs(),
    .GA2CI_guard_pcf_valid(),
    .GA2CI_guard_pcf(),
    .CI2GA_guard_pcf_ready(),
    .GA2CI_guard_timeout(),

    .GA2CCE_guard_state_cs(),
    .GA2CCE_guard_timeout(),
    .CCE2GA_guard_pcf_ready(),

    .GA2CU_guard_state_cs(),
    .GA2CU_guard_pcf_valid(),
    .GA2CU_guard_pcf(),
    .CU2GA_guard_pcf_ready(),

    .GA2CWI_guard_state_cs(),
    .GA2CWI_guard_pcf_valid(),
    .GA2CWI_guard_pcf(),
    .CWI2GA_guard_pcf_ready(),
    .GA2CWI_guard_timeout(),

    .GA2SYN_guard_state_cs(),
    .GA2SYN_local_clk(),
    .SYN2GA_guard_pcf_ready(),
    .GA2SYN_guard_stable_cycle_ctr(),
    .GA2SYN_guard_conf(),
    .GA2SYN_guard_clique_in(),

    .GA2STA_guard_state_cs(),
    .GA2STA_local_clk(),
    .STA2GA_guard_pcf_ready(),
    .GA2STA_guard_stable_cycle_ctr(),
    .GA2STA_guard_conf(),
    .GA2STA_guard_clique_in()
);
*/
