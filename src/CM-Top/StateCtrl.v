/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: StateCtrl.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module StateCtrl(
    input  wire         SYS2CORE_clk,
    input  wire         SYS2CORE_rst_n,

    input  wire [336:0] CONF2CORE_cm_static_conf,

    input  wire [ 19:0] G_local_clk,

    input  wire         TOC2SCT_time_out,

    input  wire         G_pcf_des_wr,
    input  wire [ 90:0] G_pcf_des,
    output wire         SCT2PD_pcf_des_ready,

    input  wire [  5:0] CD2SCT_clique_in,//modify by lxj 20200520 

    output wire [ 42:0] G_current_state,//modify by lxj 20200514
    output wire [298:0] CORE2MON_cm_state,
    
    output wire         state_clr_trigger//状态机中的清零条件开关
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
wire [  7:0] stable_cycle_ctr;

wire [  5:0] current_top_state;

wire         GA2CI_guard_state_cs;
wire         GA2CI_guard_pcf_valid;
wire [  2:0] GA2CI_guard_pcf;
wire         CI2GA_guard_pcf_ready;
wire         GA2CI_guard_timeout;

wire         GA2CCE_guard_state_cs;
wire         GA2CCE_guard_timeout;
wire         CCE2GA_guard_pcf_ready;

wire         GA2CU_guard_state_cs;
wire         GA2CU_guard_pcf_valid;
wire [  3:0] GA2CU_guard_pcf;
wire         CU2GA_guard_pcf_ready;

wire         GA2CWI_guard_state_cs;
wire         GA2CWI_guard_pcf_valid;
wire [  2:0] GA2CWI_guard_pcf;
wire         CWI2GA_guard_pcf_ready;
wire         GA2CWI_guard_timeout;

wire         GA2SYN_guard_state_cs;
wire [ 19:0] GA2SYN_local_clk;
wire         SYN2GA_guard_pcf_ready;
wire [  7:0] GA2SYN_guard_stable_cycle_ctr;
wire [ 68:0] GA2SYN_guard_conf;
wire [  2:0] GA2SYN_guard_clique_in;

wire         GA2STA_guard_state_cs;
wire [ 19:0] GA2STA_local_clk;
wire         STA2GA_guard_pcf_ready;
wire [  7:0] GA2STA_guard_stable_cycle_ctr;
wire [ 67:0] GA2STA_guard_conf;
wire [  2:0] GA2STA_guard_clique_in;


wire         CI2CS_cmd_valid;
wire [  5:0] CI2CS_cmd_next_top_state;
wire [  4:0] CI2CS_cmd_bottom_state;
       
wire         CCE2CS_cmd_valid;
wire [  5:0] CCE2CS_cmd_next_top_state;
wire [  3:0] CCE2CS_cmd_bottom_state;
       
wire         CU2CS_cmd_valid;
wire [  5:0] CU2CS_cmd_next_top_state;
wire [  5:0] CU2CS_cmd_bottom_state;
       
wire         CWI2CS_cmd_valid;
wire [  5:0] CWI2CS_cmd_next_top_state;
wire [  4:0] CWI2CS_cmd_bottom_state;
       
wire         SYN2CS_cmd_valid;
wire [  5:0] SYN2CS_cmd_next_top_state;
wire [  8:0] SYN2CS_cmd_bottom_state;
       
wire         STA2CS_cmd_valid;
wire [  5:0] STA2CS_cmd_next_top_state;
wire [  7:0] STA2CS_cmd_bottom_state;

wire [ 36:0] CS2CTS_bottom_state;
wire         CS2CTS_next_top_state_valid;
wire [  5:0] CS2CTS_next_top_state;
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP....

//条件分配
GuardAllocate GuardAllocate_inst(
    .G_local_clk(G_local_clk),
    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .TOC2SCT_time_out(TOC2SCT_time_out),

    .G_pcf_des_wr(G_pcf_des_wr),
    .G_pcf_des(G_pcf_des),
    .SCT2PD_pcf_des_ready(SCT2PD_pcf_des_ready),
    
    .CD2SCT_clique_in(CD2SCT_clique_in),
    .stable_cycle_ctr(stable_cycle_ctr),
    .current_top_state(current_top_state),

    .GA2CI_guard_state_cs(GA2CI_guard_state_cs),
    .GA2CI_guard_pcf_valid(GA2CI_guard_pcf_valid),
    .GA2CI_guard_pcf(GA2CI_guard_pcf),
    .CI2GA_guard_pcf_ready(CI2GA_guard_pcf_ready),
    .GA2CI_guard_timeout(GA2CI_guard_timeout),

    .GA2CCE_guard_state_cs(GA2CCE_guard_state_cs),
    .GA2CCE_guard_timeout(GA2CCE_guard_timeout),
    .CCE2GA_guard_pcf_ready(CCE2GA_guard_pcf_ready),

    .GA2CU_guard_state_cs(GA2CU_guard_state_cs),
    .GA2CU_guard_pcf_valid(GA2CU_guard_pcf_valid),
    .GA2CU_guard_pcf(GA2CU_guard_pcf),
    .CU2GA_guard_pcf_ready(CU2GA_guard_pcf_ready),

    .GA2CWI_guard_state_cs(GA2CWI_guard_state_cs),
    .GA2CWI_guard_pcf_valid(GA2CWI_guard_pcf_valid),
    .GA2CWI_guard_pcf(GA2CWI_guard_pcf),
    .CWI2GA_guard_pcf_ready(CWI2GA_guard_pcf_ready),
    .GA2CWI_guard_timeout(GA2CWI_guard_timeout),

    .GA2SYN_guard_state_cs(GA2SYN_guard_state_cs),
    .GA2SYN_local_clk(GA2SYN_local_clk),
    .SYN2GA_guard_pcf_ready(SYN2GA_guard_pcf_ready),
    .GA2SYN_guard_stable_cycle_ctr(GA2SYN_guard_stable_cycle_ctr),
    .GA2SYN_guard_conf(GA2SYN_guard_conf),
    .GA2SYN_guard_clique_in(GA2SYN_guard_clique_in),

    .GA2STA_guard_state_cs(GA2STA_guard_state_cs),
    .GA2STA_local_clk(GA2STA_local_clk),
    .STA2GA_guard_pcf_ready(STA2GA_guard_pcf_ready),
    .GA2STA_guard_stable_cycle_ctr(GA2STA_guard_stable_cycle_ctr),
    .GA2STA_guard_conf(GA2STA_guard_conf),
    .GA2STA_guard_clique_in(GA2STA_guard_clique_in)
);

//主状态
CMTopState CMTopState_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .stable_cycle_ctr(stable_cycle_ctr),
    .current_top_state(current_top_state),

    .CS2CTS_next_top_state_valid(CS2CTS_next_top_state_valid),
    .CS2CTS_next_top_state(CS2CTS_next_top_state),
    .CS2CTS_bottom_state(CS2CTS_bottom_state)
);

CMIntegrate CMIntegrate_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .GA2CI_guard_state_cs(GA2CI_guard_state_cs),
    .GA2CI_guard_pcf_valid(GA2CI_guard_pcf_valid),
    .GA2CI_guard_pcf(GA2CI_guard_pcf),
    .CI2GA_guard_pcf_ready(CI2GA_guard_pcf_ready),
    
    .GA2CI_guard_timeout(GA2CI_guard_timeout),

    .CI2CS_cmd_valid(CI2CS_cmd_valid),
    .CI2CS_cmd_next_top_state(CI2CS_cmd_next_top_state),
    .CI2CS_cmd_bottom_state(CI2CS_cmd_bottom_state)
);

CMUnsync CMUnsync_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .GA2CU_guard_state_cs(GA2CU_guard_state_cs),
    
    .GA2CU_guard_pcf_valid(GA2CU_guard_pcf_valid),
    .GA2CU_guard_pcf(GA2CU_guard_pcf),
    .CU2GA_guard_pcf_ready(CU2GA_guard_pcf_ready),

    .CU2CS_cmd_valid(CU2CS_cmd_valid),
    .CU2CS_cmd_next_top_state(CU2CS_cmd_next_top_state),
    .CU2CS_cmd_bottom_state(CU2CS_cmd_bottom_state)
);

CMCAEnabled CMCAEnabled_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .GA2CCE_guard_state_cs(GA2CCE_guard_state_cs),
    
    .GA2CCE_guard_timeout(GA2CCE_guard_timeout),
    
    .CCE2GA_guard_pcf_ready(CCE2GA_guard_pcf_ready),

    .CCE2CS_cmd_valid(CCE2CS_cmd_valid),
    .CCE2CS_cmd_next_top_state(CCE2CS_cmd_next_top_state),
    .CCE2CS_cmd_bottom_state(CCE2CS_cmd_bottom_state)
);

CMWait4In CMWait4In_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .GA2CWI_guard_state_cs(GA2CWI_guard_state_cs),
    
    .GA2CWI_guard_pcf_valid(GA2CWI_guard_pcf_valid),
    .GA2CWI_guard_pcf(GA2CWI_guard_pcf),
    .CWI2GA_guard_pcf_ready(CWI2GA_guard_pcf_ready),
    
    .GA2CWI_guard_timeout(GA2CWI_guard_timeout),

    .CWI2CS_cmd_valid(CWI2CS_cmd_valid),
    .CWI2CS_cmd_next_top_state(CWI2CS_cmd_next_top_state),
    .CWI2CS_cmd_bottom_state(CWI2CS_cmd_bottom_state)
);

CMSync CMSync_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),
    
    .GA2SYN_local_clk(GA2SYN_local_clk),
    
    .GA2SYN_guard_state_cs(GA2SYN_guard_state_cs),

    .SYN2GA_guard_pcf_ready(SYN2GA_guard_pcf_ready),
    
    .GA2SYN_guard_stable_cycle_ctr(GA2SYN_guard_stable_cycle_ctr),
    
    .GA2SYN_guard_conf(GA2SYN_guard_conf),
    .GA2SYN_guard_clique_in(GA2SYN_guard_clique_in),

    .SYN2CS_cmd_valid(SYN2CS_cmd_valid),
    .SYN2CS_cmd_next_top_state(SYN2CS_cmd_next_top_state),
    .SYN2CS_cmd_bottom_state(SYN2CS_cmd_bottom_state)
);

CMStable CMStable_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .GA2STA_guard_state_cs(GA2STA_guard_state_cs),
    
    .GA2STA_local_clk(GA2STA_local_clk),
    
    .STA2GA_guard_pcf_ready(STA2GA_guard_pcf_ready),
    
    .GA2STA_guard_stable_cycle_ctr(GA2STA_guard_stable_cycle_ctr),
    
    .GA2STA_guard_conf(GA2STA_guard_conf),
    .GA2STA_guard_clique_in(GA2STA_guard_clique_in),

    .STA2CS_cmd_valid(STA2CS_cmd_valid),
    .STA2CS_cmd_next_top_state(STA2CS_cmd_next_top_state),
    .STA2CS_cmd_bottom_state(STA2CS_cmd_bottom_state)
);

//命令选择
CommandSelect CommandSelect_inst(
    .SYS2CORE_clk(SYS2CORE_clk),

    .stable_cycle_ctr(stable_cycle_ctr),
    .current_top_state(current_top_state),
    
    .G_pcf_des_wr(G_pcf_des_wr),
    .G_pcf_des(G_pcf_des),

    .CI2CS_cmd_valid(CI2CS_cmd_valid),
    .CI2CS_cmd_next_top_state(CI2CS_cmd_next_top_state),
    .CI2CS_cmd_bottom_state(CI2CS_cmd_bottom_state),

    .CCE2CS_cmd_valid(CCE2CS_cmd_valid),
    .CCE2CS_cmd_next_top_state(CCE2CS_cmd_next_top_state),
    .CCE2CS_cmd_bottom_state(CCE2CS_cmd_bottom_state),

    .CU2CS_cmd_valid(CU2CS_cmd_valid),
    .CU2CS_cmd_next_top_state(CU2CS_cmd_next_top_state),
    .CU2CS_cmd_bottom_state(CU2CS_cmd_bottom_state),

    .CWI2CS_cmd_valid(CWI2CS_cmd_valid),
    .CWI2CS_cmd_next_top_state(CWI2CS_cmd_next_top_state),
    .CWI2CS_cmd_bottom_state(CWI2CS_cmd_bottom_state),

    .SYN2CS_cmd_valid(SYN2CS_cmd_valid),
    .SYN2CS_cmd_next_top_state(SYN2CS_cmd_next_top_state),
    .SYN2CS_cmd_bottom_state(SYN2CS_cmd_bottom_state),

    .STA2CS_cmd_valid(STA2CS_cmd_valid),
    .STA2CS_cmd_next_top_state(STA2CS_cmd_next_top_state),
    .STA2CS_cmd_bottom_state(STA2CS_cmd_bottom_state),

    .CS2CTS_next_top_state_valid(CS2CTS_next_top_state_valid),
    .CS2CTS_next_top_state(CS2CTS_next_top_state),
    .CS2CTS_bottom_state(CS2CTS_bottom_state),
    
    .G_current_state(G_current_state),
    
    .CORE2MON_cm_state(CORE2MON_cm_state),

    .state_clr_trigger(state_clr_trigger)//状态机中的清零条件开关
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);
endmodule
/*
StateCtrl StateCtrl_inst(
    .SYS2CORE_clk(),
    .SYS2CORE_rst_n(),

    .CONF2CORE_cm_static_conf(),

    .G_local_clk(),

    .TOC2SCT_time_out(),

    .G_pcf_des_wr(),
    .G_pcf_des(),
    .SCT2PD_pcf_des_ready(),

    .CD2SCT_clique_in(),

    .G_current_state(),
    .CORE2MON_cm_state(),

    .state_clr_trigger()//状态机中的清零条件开关
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);
*/