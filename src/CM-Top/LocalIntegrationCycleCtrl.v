/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: LocalIntegrationCycleCtrl.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

`timescale 1ns/1ps
module LocalIntegrationCycleCtrl(
    input  wire         SYS2CORE_clk,
    input  wire         SYS2CORE_rst_n,
    
    output reg  [  5:0] G_Integration_Cycle,
    
    input  wire [336:0] CONF2CORE_cm_static_conf,
    
    input  wire         LCC2LICC_plus_1,

    input  wire         G_pcf_des_wr,
    input  wire [ 90:0] G_pcf_des,
    
    output wire [298:0] CORE2MON_cm_state,
    
    input  wire         state_clr_trigger//状态机中的清零条件
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
reg [5:0] next_Integration_Cycle;

//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
localparam CM_INTEGRATE  = 6'b000001,
           CM_UNSYNC     = 6'b000010,
           CM_CA_ENABLED = 6'b000100,
           CM_WAIT_4_IN  = 6'b001000,
           CM_SYNC       = 6'b010000,
           CM_STABLE     = 6'b100000;
//***************************************************
//              状态统计信号
//***************************************************          
assign CORE2MON_cm_state = {G_Integration_Cycle,293'b0};
//***************************************************
//              G_Integration_Cycle处理
//***************************************************  
always @* begin
    if((state_clr_trigger == 1'b1) || 
       ((G_Integration_Cycle+LCC2LICC_plus_1) == CONF2CORE_cm_static_conf[254:249])
      ) begin//modify by lxj 20200430
        next_Integration_Cycle = 6'b0;
    end
    else if((G_pcf_des_wr == 1'b1) && (G_pcf_des[40:38] > 3'b0)) begin
        next_Integration_Cycle = G_pcf_des[7:2];
    end
    else begin
        next_Integration_Cycle = G_Integration_Cycle+LCC2LICC_plus_1;
    end
end

always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(~SYS2CORE_rst_n) begin
        G_Integration_Cycle <= 6'b0;//modify by lxj 20200514
    end
    else begin
        G_Integration_Cycle <= next_Integration_Cycle;
    end
end
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP.... 
endmodule
/*
LocalIntegrationCycleCtrl LocalIntegrationCycleCtrl_inst(
    .SYS2CORE_clk(),
    .SYS2CORE_rst_n(),

    .G_Integration_Cycle(),

    .CONF2CORE_cm_static_conf(),

    .LCC2LICC_plus_1(),

    .G_pcf_des_wr(),
    .G_pcf_des(),

    .CORE2MON_cm_state(),

    .state_clr_trigger()//状态机中的清零条件
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);
*/