/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: CMSync.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module CMSync(
    input  wire        SYS2CORE_clk,
    input  wire        SYS2CORE_rst_n,
    
    input  wire [19:0] GA2SYN_local_clk,
    
    input  wire        GA2SYN_guard_state_cs,

    output wire        SYN2GA_guard_pcf_ready,
    
    input  wire [ 7:0] GA2SYN_guard_stable_cycle_ctr,
    
    input  wire [68:0] GA2SYN_guard_conf,    
    input  wire [ 2:0] GA2SYN_guard_clique_in, 
    
    output reg         SYN2CS_cmd_valid,
    output reg  [5:0]  SYN2CS_cmd_next_top_state,
    output wire [8:0]  SYN2CS_cmd_bottom_state
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
reg [8:0] next_syn_state;
reg [8:0] curr_syn_state;

localparam CM_INTEGRATE  = 6'b000001,
           CM_UNSYNC     = 6'b000010,
           CM_CA_ENABLED = 6'b000100,
           CM_WAIT_4_IN  = 6'b001000,
           CM_SYNC       = 6'b010000,
           CM_STABLE     = 6'b100000;
           
localparam INIT_S     = 9'b000000001,
           MATCH_S    = 9'b000000010,
           COMPLETE_S = 9'b000000100,
           C0_S       = 9'b000001000,
           C1_S       = 9'b000010000,
           C2_S       = 9'b000100000,
           C3_S       = 9'b001000000,
           C4_S       = 9'b010000000,
           C5_S       = 9'b100000000;
/*//////////////////////////////////////////////////////////
                    状态机跳转处理
*///////////////////////////////////////////////////////////
assign SYN2CS_cmd_bottom_state = next_syn_state;

assign SYN2GA_guard_pcf_ready = (next_syn_state == MATCH_S);


always @* begin
    case(curr_syn_state)
        INIT_S: begin
            if(GA2SYN_guard_state_cs == 1'b1)
                next_syn_state = MATCH_S;
            else
                next_syn_state = INIT_S;
        end
        
        MATCH_S: begin//modify by lxj 20200427
            if((GA2SYN_local_clk == GA2SYN_guard_conf[39:20]) && 
               (GA2SYN_guard_clique_in[2] == 1'd1))
                next_syn_state = C0_S;
            else if((GA2SYN_local_clk == GA2SYN_guard_conf[59:40]) && 
                    (GA2SYN_guard_clique_in[0] == 1'd1))
                next_syn_state = C1_S;
            else if((GA2SYN_local_clk == GA2SYN_guard_conf[59:40]) && 
                    (GA2SYN_guard_clique_in[1] == 1'd1) && 
                    (GA2SYN_guard_conf[68] == 1'b1) && 
                    (GA2SYN_guard_stable_cycle_ctr < GA2SYN_guard_conf[67:60]))
                next_syn_state = C3_S;
            else if((GA2SYN_local_clk == GA2SYN_guard_conf[59:40]) && 
                    (GA2SYN_guard_clique_in[1] == 1'd1) && 
                    (GA2SYN_guard_conf[68] == 1'b1) && 
                    (GA2SYN_guard_stable_cycle_ctr >= GA2SYN_guard_conf[67:60]))
                next_syn_state = C4_S;
            else if((GA2SYN_local_clk == GA2SYN_guard_conf[59:40]) && 
                    (GA2SYN_guard_clique_in[1] == 1'd1))//必须C3和C4判断之后
                next_syn_state = C2_S;
            else if(GA2SYN_local_clk == GA2SYN_guard_conf[19:0])
                next_syn_state = C5_S;
            else
                next_syn_state = MATCH_S;
        end
        
        C0_S: begin
            next_syn_state = COMPLETE_S;
        end
        
        C1_S: begin
            next_syn_state = COMPLETE_S;
        end
        
        C2_S: begin
            next_syn_state = COMPLETE_S;
        end
        
        C3_S: begin
            next_syn_state = COMPLETE_S;
        end
        
        C4_S: begin
            next_syn_state = COMPLETE_S;
        end
        
        C5_S: begin
            next_syn_state = COMPLETE_S;
        end
        
        COMPLETE_S: begin
            next_syn_state = INIT_S;
        end
        
        default: next_syn_state = INIT_S;
    endcase
end

//时序逻辑用于缓存当前状态
always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(SYS2CORE_rst_n == 1'b0) begin
        curr_syn_state <= INIT_S;
    end
    else begin
        curr_syn_state <= next_syn_state;
    end
end

//在该时序逻辑部分实现各状态的执行动作
always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(SYS2CORE_rst_n == 1'b0) begin
        SYN2CS_cmd_valid <= 1'b0;
        SYN2CS_cmd_next_top_state <= 6'b0;
    end
    else begin
        case(curr_syn_state)
            INIT_S: begin
                SYN2CS_cmd_valid <= 1'b0;
                SYN2CS_cmd_next_top_state <= 6'b0;
            end
            
            MATCH_S:begin
                SYN2CS_cmd_valid <= 1'b0;
                SYN2CS_cmd_next_top_state <= 6'b0;
            end
            
            C0_S:begin
                SYN2CS_cmd_valid <= 1'b1;
                SYN2CS_cmd_next_top_state <= CM_INTEGRATE;
            end
            
            C1_S:begin
                SYN2CS_cmd_valid <= 1'b1;
                SYN2CS_cmd_next_top_state <= CM_INTEGRATE;
            end
            
            C2_S:begin
                SYN2CS_cmd_valid <= 1'b1;
                SYN2CS_cmd_next_top_state <= CM_SYNC;
            end
            
            C3_S:begin
                SYN2CS_cmd_valid <= 1'b1;
                SYN2CS_cmd_next_top_state <= CM_SYNC;
            end
            
            C4_S:begin
                SYN2CS_cmd_valid <= 1'b1;
                SYN2CS_cmd_next_top_state <= CM_STABLE;
            end
            
            C5_S:begin
                SYN2CS_cmd_valid <= 1'b1;
                SYN2CS_cmd_next_top_state <= CM_SYNC;
            end
            
            COMPLETE_S:begin
                SYN2CS_cmd_valid <= 1'b0;
                SYN2CS_cmd_next_top_state <= 6'b0;
            end
            
            default: begin
                SYN2CS_cmd_valid <= 1'b0;
                SYN2CS_cmd_next_top_state <= 6'b0;
            end
        endcase
    end
end
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP....   
endmodule
/*
CMSync CMSync_inst(
    .SYS2CORE_clk(),
    .SYS2CORE_rst_n(),

    .GA2SYN_local_clk(),

    .GA2SYN_guard_state_cs(),

    .SYN2GA_guard_pcf_ready(),

    .GA2SYN_guard_stable_cycle_ctr(),

    .GA2SYN_guard_conf(),    
    .GA2SYN_guard_clique_in(), 

    .SYN2CS_cmd_valid(),
    .SYN2CS_cmd_next_top_state(),
    .SYN2CS_cmd_bottom_state()
);
*/