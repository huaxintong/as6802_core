/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: CMTop.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module CMTop(
    input  wire         SYS2CORE_clk,
    input  wire         SYS2CORE_rst_n,
    
    input  wire [ 47:0] G_device_clk,
    
    input  wire [336:0] CONF2CORE_cm_static_conf,
    input  wire [ 27:0] CONF2CORE_dbg_conf,//add by lxj 20200526
    output wire [ 19:0] CM2LCPM_local_clk,

    input  wire         PPM2SCM_pcf_rx_cb_wr,
    input  wire [ 85:0] PPM2SCM_pcf_rx_cb,
    output wire         SCM2PPM_pcf_rx_cb_ready,
    
    output wire         CM2OPPM_pcf_tx_cb_wr,
    output wire [ 63:0] CM2OPPM_pcf_tx_cb,
    input  wire         CM2OPPM_pcf_tx_cb_ready,
    
    output wire [298:0] CORE2MON_cm_state
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明
wire [ 19:0] G_local_clk;
wire [  5:0] G_Integration_Cycle;

wire         CC2PP_compress_end;
wire [ 42:0] G_current_state;

wire         PP2PD_pcf_rx_cb_wr;
wire [ 85:0] PP2PD_pcf_rx_cb;
wire         PD2PP_pcf_rx_cb_ready;

wire         PP2CC_pcf_rx_cb_wrreq;
wire [ 85:0] PP2CC_pcf_rx_cb;
wire         CC2PP_pcf_rx_cb_wrack;

wire         CC2PD_pcf_rx_cb_wr;
wire [ 85:0] CC2PD_pcf_rx_cb;
wire         PD2CC_pcf_rx_cb_ready;

wire [ 90:0] G_pcf_des;
wire         G_pcf_des_wr;
wire         SCT2PD_pcf_des_ready;

wire [ 19:0] SCP2LCC_clock_corr;

wire         SCP2CD_sync_des_valid;
wire [ 17:0] SCP2CD_sync_des;

wire [  5:0] CD2SCT_clique_in;
wire         state_clr_trigger;//状态机中的清零条件
//针对local_clk/local_integrate_cycle/local_sync/async_membership

wire         TOC2SCT_time_out;

wire         LCC2LICC_plus_1;

wire [298:0] cm_SCP_state;
wire [298:0] cm_CD_state;
wire [298:0] cm_SCT_state;
wire [298:0] cm_TOC_state;
wire [298:0] cm_LCC_state;
wire [298:0] cm_LICC_state;
wire [298:0] cm_PES_state;
/*//////////////////////////////////////////////////////////
                   杂类信号整合
*///////////////////////////////////////////////////////////
assign CORE2MON_cm_state = {
    cm_LICC_state[298:293],
     cm_LCC_state[292:273],
     cm_TOC_state[272:253], 
     cm_TOC_state[252:252],
     cm_SCP_state[251:232],
      cm_CD_state[231:224],
      cm_CD_state[223:216],
      cm_CD_state[215:208],
     cm_SCT_state[207:200],
     cm_SCT_state[199:199],
     cm_SCT_state[198:108],
     cm_PES_state[107:107],
     cm_PES_state[106: 43],
     cm_SCT_state[ 42: 37],
     cm_SCT_state[ 36: 32],
     cm_SCT_state[ 31: 26],
     cm_SCT_state[ 25: 22],
     cm_SCT_state[ 21: 17],
     cm_SCT_state[ 16:  8],
     cm_SCT_state[  7:  0]
};

assign CM2LCPM_local_clk = G_local_clk;
/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP.... 
PermntProc PermntProc_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .G_device_clk(G_device_clk),
    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .PPM2SCM_pcf_rx_cb_wr(PPM2SCM_pcf_rx_cb_wr),
    .PPM2SCM_pcf_rx_cb(PPM2SCM_pcf_rx_cb),
    .SCM2PPM_pcf_rx_cb_ready(SCM2PPM_pcf_rx_cb_ready),

    .PP2PD_pcf_rx_cb_wr(PP2PD_pcf_rx_cb_wr),
    .PP2PD_pcf_rx_cb(PP2PD_pcf_rx_cb),
    .PD2PP_pcf_rx_cb_ready(PD2PP_pcf_rx_cb_ready),

    .PP2CC_pcf_rx_cb_wrreq(PP2CC_pcf_rx_cb_wrreq),
    .PP2CC_pcf_rx_cb(PP2CC_pcf_rx_cb),
    .CC2PP_pcf_rx_cb_wrack(CC2PP_pcf_rx_cb_wrack),

    .CC2PP_compress_end(CC2PP_compress_end)
);

CompressCtrl CompressCtrl_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .CC2PP_compress_end(CC2PP_compress_end),

    .PP2CC_pcf_rx_cb_wrreq(PP2CC_pcf_rx_cb_wrreq),
    .PP2CC_pcf_rx_cb(PP2CC_pcf_rx_cb),
    .CC2PP_pcf_rx_cb_wrack(CC2PP_pcf_rx_cb_wrack),

    .CC2PD_pcf_rx_cb_wr(CC2PD_pcf_rx_cb_wr),
    .CC2PD_pcf_rx_cb(CC2PD_pcf_rx_cb),
    .PD2CC_pcf_rx_cb_ready(PD2CC_pcf_rx_cb_ready)
);

PitDelay PitDelay_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .G_local_clk(G_local_clk),
    .G_device_clk(G_device_clk),
    
    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),
    .G_current_state(G_current_state),
    
    .PP2PD_pcf_rx_cb_wr(PP2PD_pcf_rx_cb_wr),
    .PP2PD_pcf_rx_cb(PP2PD_pcf_rx_cb),
    .PD2PP_pcf_rx_cb_ready(PD2PP_pcf_rx_cb_ready),

    .CC2PD_pcf_rx_cb_wr(CC2PD_pcf_rx_cb_wr),
    .CC2PD_pcf_rx_cb(CC2PD_pcf_rx_cb),
    .PD2CC_pcf_rx_cb_ready(PD2CC_pcf_rx_cb_ready),

    .G_pcf_des(G_pcf_des),
    .G_pcf_des_wr(G_pcf_des_wr),
    .SCT2PD_pcf_des_ready(SCT2PD_pcf_des_ready) 
);

SyncCompute SyncCompute_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),
    
    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),
    .CONF2CORE_dbg_conf(CONF2CORE_dbg_conf),//add by lxj 20200526
    .G_local_clk(G_local_clk),
    .G_Integration_Cycle(G_Integration_Cycle),

    .G_pcf_des_wr(G_pcf_des_wr),
    .G_pcf_des(G_pcf_des),

    .SCP2LCC_clock_corr(SCP2LCC_clock_corr),

    .SCP2CD_sync_des_valid(SCP2CD_sync_des_valid),
    .SCP2CD_sync_des(SCP2CD_sync_des),

    .CORE2MON_cm_state(cm_SCP_state)
);

CliqueDetect CliqueDetect_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),
    
    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .G_local_clk(G_local_clk),
    .G_Integration_Cycle(G_Integration_Cycle),

    .SCP2CD_sync_des_valid(SCP2CD_sync_des_valid),
    .SCP2CD_sync_des(SCP2CD_sync_des),

    .CD2SCT_clique_in(CD2SCT_clique_in),

    .state_clr_trigger(state_clr_trigger),//状态机中的清零条件
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
    
    .CORE2MON_cm_state(cm_CD_state)
);

StateCtrl StateCtrl_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),
    
    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .G_local_clk(G_local_clk),

    .TOC2SCT_time_out(TOC2SCT_time_out),

    .G_pcf_des(G_pcf_des),
    .G_pcf_des_wr(G_pcf_des_wr),
    .SCT2PD_pcf_des_ready(SCT2PD_pcf_des_ready),

    .CD2SCT_clique_in(CD2SCT_clique_in),

    .G_current_state(G_current_state),
    .CORE2MON_cm_state(cm_SCT_state),

    .state_clr_trigger(state_clr_trigger)//状态机中的清零条件开关
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);

TimeOutControl TimeOutControl_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),
    
    .G_current_state(G_current_state),
    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .TOC2SCT_time_out(TOC2SCT_time_out),

    .CORE2MON_cm_state(cm_TOC_state)
);

LocalClockCtrl LocalClockCtrl_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .G_local_clk(G_local_clk),

    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),
    .G_current_state(G_current_state),

    .G_pcf_des_wr(G_pcf_des_wr),
    .G_pcf_des(G_pcf_des),

    .SCP2LCC_clock_corr(SCP2LCC_clock_corr),

    .CORE2MON_cm_state(cm_LCC_state),

    .LCC2LICC_plus_1(LCC2LICC_plus_1),
    .state_clr_trigger(state_clr_trigger)//状态机中的清零条件
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);

LocalIntegrationCycleCtrl LocalIntegrationCycleCtrl_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .G_Integration_Cycle(G_Integration_Cycle),

    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .LCC2LICC_plus_1(LCC2LICC_plus_1),

    .G_pcf_des_wr(G_pcf_des_wr),
    .G_pcf_des(G_pcf_des),

    .CORE2MON_cm_state(cm_LICC_state),

    .state_clr_trigger(state_clr_trigger)//状态机中的清零条件
    //针对local_clk/local_integrate_cycle/local_sync/async_membership
);

PcfEventSend PcfEventSend_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .G_device_clk(G_device_clk),
    .G_current_state(G_current_state),

    .G_pcf_des_wr(G_pcf_des_wr),
    .G_pcf_des(G_pcf_des),

    .CM2OPPM_pcf_tx_cb_wr(CM2OPPM_pcf_tx_cb_wr),
    .CM2OPPM_pcf_tx_cb(CM2OPPM_pcf_tx_cb),
    .CM2OPPM_pcf_tx_cb_ready(CM2OPPM_pcf_tx_cb_ready),
    
    .CORE2MON_cm_state(cm_PES_state)
);
endmodule
/*
CMTop CMTop_inst(
    .SYS2CORE_clk(),
    .SYS2CORE_rst_n(),

    .G_device_clk(),
    .CONF2CORE_cm_static_conf(),

    .CM2LCPM_local_clk(),

    .PPM2SCM_pcf_rx_cb_wr(),
    .PPM2SCM_pcf_rx_cb(),
    .SCM2PPM_pcf_rx_cb_ready(),

    .CM2OPPM_pcf_tx_cb_wr(),
    .CM2OPPM_pcf_tx_cb(),
    .CM2OPPM_pcf_tx_cb_ready(),

    .CORE2MON_cm_state()
);
*/