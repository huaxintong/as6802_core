/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: PermntProc.v
 *Target Device: Altera
 *Author : 刘晓骏
*/

module PermntProc(
    input  wire         SYS2CORE_clk,
    input  wire         SYS2CORE_rst_n,

    input  wire [ 47:0] G_device_clk,
    input  wire [336:0] CONF2CORE_cm_static_conf,

    input  wire         PPM2SCM_pcf_rx_cb_wr,
    input  wire [ 85:0] PPM2SCM_pcf_rx_cb,
    output wire         SCM2PPM_pcf_rx_cb_ready,

    output wire         PP2PD_pcf_rx_cb_wr,
    output wire [ 85:0] PP2PD_pcf_rx_cb,
    input  wire         PD2PP_pcf_rx_cb_ready,

    output wire         PP2CC_pcf_rx_cb_wrreq,
    output wire [ 85:0] PP2CC_pcf_rx_cb,
    input  wire         CC2PP_pcf_rx_cb_wrack,

    input  wire         CC2PP_compress_end
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
wire [  3:0] PC2PAC_pcf_rx_cb_wr;
wire [ 85:0] PC2PAC_pcf_rx_cb;
wire [  3:0] PAC2PC_pcf_rx_cb_ready;

wire [  3:0] PAC2PC_array_empty;

wire         PAC2CPS_pcf_rx_cb_wr[3:0];
wire [ 85:0] PAC2CPS_pcf_rx_cb[3:0];
wire         CPS2PAC_pcf_rx_cb_ready[3:0];

/*//////////////////////////////////////////////////////////
                   IP调用区域
*///////////////////////////////////////////////////////////
//本模块调用的所有IP在该区域实例化
//例如fifo/ram/grant之类的IP.... 
PCFClassify PCFClassify_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .PPM2SCM_pcf_rx_cb_wr(PPM2SCM_pcf_rx_cb_wr),
    .PPM2SCM_pcf_rx_cb(PPM2SCM_pcf_rx_cb),
    .SCM2PPM_pcf_rx_cb_ready(SCM2PPM_pcf_rx_cb_ready),

    .PP2PD_pcf_rx_cb_wr(PP2PD_pcf_rx_cb_wr),
    .PP2PD_pcf_rx_cb(PP2PD_pcf_rx_cb),
    .PD2PP_pcf_rx_cb_ready(PD2PP_pcf_rx_cb_ready),

    .PC2PAC_pcf_rx_cb_wr(PC2PAC_pcf_rx_cb_wr),
    .PC2PAC_pcf_rx_cb(PC2PAC_pcf_rx_cb),
    .PAC2PC_pcf_rx_cb_ready(PAC2PC_pcf_rx_cb_ready),

    .PAC2PC_array_empty(PAC2PC_array_empty)
);

generate
    genvar i;
    for(i=0;i<4;i=i+1) begin:Array
        PCFArrayCtrl PCFArrayCtrl_inst(
            .SYS2CORE_clk(SYS2CORE_clk),
            .SYS2CORE_rst_n(SYS2CORE_rst_n),

            .G_device_clk(G_device_clk),

            .PC2PAC_pcf_rx_cb_wr(PC2PAC_pcf_rx_cb_wr[i]),
            .PC2PAC_pcf_rx_cb(PC2PAC_pcf_rx_cb),
            .PAC2PC_pcf_rx_cb_ready(PAC2PC_pcf_rx_cb_ready[i]),

            .PAC2CPS_pcf_rx_cb_wr(PAC2CPS_pcf_rx_cb_wr[i]),
            .PAC2CPS_pcf_rx_cb(PAC2CPS_pcf_rx_cb[i]),
            .CPS2PAC_pcf_rx_cb_ready(CPS2PAC_pcf_rx_cb_ready[i]),
            
            .PAC2PC_array_empty(PAC2PC_array_empty[i])
        );
    end
endgenerate

CompressPCFSelect CompressPCFSelect_inst(
    .SYS2CORE_clk(SYS2CORE_clk),
    .SYS2CORE_rst_n(SYS2CORE_rst_n),

    .G_device_clk(G_device_clk),

    .CONF2CORE_cm_static_conf(CONF2CORE_cm_static_conf),

    .CC2PP_compress_end(CC2PP_compress_end),

    .PAC2CPS_pcf_rx_cb_wr_0(PAC2CPS_pcf_rx_cb_wr[0]),
    .PAC2CPS_pcf_rx_cb_0(PAC2CPS_pcf_rx_cb[0]),
    .CPS2PAC_pcf_rx_cb_ready_0(CPS2PAC_pcf_rx_cb_ready[0]),

    .PAC2CPS_pcf_rx_cb_wr_1(PAC2CPS_pcf_rx_cb_wr[1]),
    .PAC2CPS_pcf_rx_cb_1(PAC2CPS_pcf_rx_cb[1]),
    .CPS2PAC_pcf_rx_cb_ready_1(CPS2PAC_pcf_rx_cb_ready[1]),

    .PAC2CPS_pcf_rx_cb_wr_2(PAC2CPS_pcf_rx_cb_wr[2]),
    .PAC2CPS_pcf_rx_cb_2(PAC2CPS_pcf_rx_cb[2]),
    .CPS2PAC_pcf_rx_cb_ready_2(CPS2PAC_pcf_rx_cb_ready[2]),

    .PAC2CPS_pcf_rx_cb_wr_3(PAC2CPS_pcf_rx_cb_wr[3]),
    .PAC2CPS_pcf_rx_cb_3(PAC2CPS_pcf_rx_cb[3]),
    .CPS2PAC_pcf_rx_cb_ready_3(CPS2PAC_pcf_rx_cb_ready[3]),

    .PP2CC_pcf_rx_cb_wrreq(PP2CC_pcf_rx_cb_wrreq),
    .PP2CC_pcf_rx_cb(PP2CC_pcf_rx_cb),
    .CC2PP_pcf_rx_cb_wrack(CC2PP_pcf_rx_cb_wrack)  
);
endmodule
/*
PermntProc PermntProc_inst(
    .SYS2CORE_clk(),
    .SYS2CORE_rst_n(),

    .G_device_clk(),
    .CONF2CORE_cm_static_conf(),

    .PPM2SCM_pcf_rx_cb_wr(),
    .PPM2SCM_pcf_rx_cb(),
    .SCM2PPM_pcf_rx_cb_ready(),

    .PP2PD_pcf_rx_cb_wr(),
    .PP2PD_pcf_rx_cb(),
    .PD2PP_pcf_rx_cb_ready(),

    .PP2CC_pcf_rx_cb_wrreq(),
    .PP2CC_pcf_rx_cb(),
    .CC2PP_pcf_rx_cb_wrack(),

    .CC2PP_compress_end()
);
*/