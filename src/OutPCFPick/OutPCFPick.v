/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: OutPCFPick.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

`timescale 1ns/1ps

module OutPCFPick(
	input wire [37:0] CONF2CORE_sys_conf,
	//sm-top
	input wire        SM2OPPM_pcf_tx_cb_wr,
	input wire  [63:0]  SM2OPPM_pcf_tx_cb,
	output  wire         SM2OPPM_pcf_tx_cb_ready,
    //cm-top
	input wire        CM2OPPM_pcf_tx_cb_wr,
	input wire  [63:0]  CM2OPPM_pcf_tx_cb,
	output  wire         CM2OPPM_pcf_tx_cb_ready,
	//pdm
	output wire OPPM2PDM_pcf_tx_cb_wr,
	output wire [63:0] OPPM2PDM_pcf_tx_cb,
	input wire OPPM2PDM_pcf_tx_cb_ready
    );


assign  SM2OPPM_pcf_tx_cb_ready = OPPM2PDM_pcf_tx_cb_ready;
assign  CM2OPPM_pcf_tx_cb_ready = OPPM2PDM_pcf_tx_cb_ready;


assign 	OPPM2PDM_pcf_tx_cb_wr = (CONF2CORE_sys_conf[21:20] == 2'b10)?SM2OPPM_pcf_tx_cb_wr:((CONF2CORE_sys_conf[21:20] == 2'b11)?CM2OPPM_pcf_tx_cb_wr:1'b0);
assign 	OPPM2PDM_pcf_tx_cb =  (CONF2CORE_sys_conf[21:20] == 2'b10)?SM2OPPM_pcf_tx_cb:((CONF2CORE_sys_conf[21:20] == 2'b11)?CM2OPPM_pcf_tx_cb:64'b0);

endmodule
