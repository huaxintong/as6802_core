/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: LocalClkPick.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

`timescale 1ns/1ps

module LocalClkPick(
    //input wire SYS2CORE_clk,
	//input wire SYS2CORE_rst_n,
	
	input wire [37:0] CONF2CORE_sys_conf,
	
	input wire [19:0] SM2LCPM_local_clk,
	input wire [19:0] CM2LCPM_local_clk,
	//input wire [19:0] SC2LCPM_local_clk,
	output wire [19:0] CORE2SYS_local_clk
    );


//assign CORE2SYS_local_clk = (CONF2CORE_sys_conf[21:20] == 2'b10)?
//							SM2LCPM_local_clk:((CONF2CORE_sys_conf[21:20] == 2'b11)?
//							CM2LCPM_local_clk:SC2LCPM_local_clk);
	
assign CORE2SYS_local_clk = (CONF2CORE_sys_conf[21:20] == 2'b10)?SM2LCPM_local_clk:CM2LCPM_local_clk;

endmodule
