/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMLocalIntegrationCycleControl.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

`timescale 1ns/1ps
module SMLocalIntegrationCycleControl(
    input  wire         SYS2CORE_clk,
    input  wire         SYS2CORE_rst_n,
    input  wire [76:0]  G_current_state,
    input  wire [348:0] CONF2CORE_sm_static_conf,
	
    input  wire [90:0]  G_pcf_des,
	input  wire         G_pcf_des_valid,
    
	input  wire           LCC2LICC_plus_1,
	
	output wire [5:0]  G_integration_cycle
    
);
//***************************************************
//            Intermediate variable Declaration
//***************************************************
//all wire/reg/parameter variable 
//should be declare below here 

reg [5:0] local_integration_cycle;

/****OutIC****/
assign  G_integration_cycle[5:0] = local_integration_cycle[5:0];

always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(~SYS2CORE_rst_n) begin
		local_integration_cycle[5:0] <= 6'b0;
    end
    else begin
		/**LICUpdate**/
		if(LCC2LICC_plus_1 == 1'b1) begin
			local_integration_cycle[5:0] <= (G_integration_cycle[5:0]+ 6'b1)==CONF2CORE_sm_static_conf[348:343]?6'b0:(G_integration_cycle[5:0]+ 6'b1);
		end
		///*Update the value of integration_cycle based on G_pcf_des and type=IN、SM_INTEGRATE、SM_UNSYNC*/ // 
		if(G_pcf_des_valid == 1'b1 && G_pcf_des[17:16] == 2'b10 && (G_current_state[76:69] == 8'b0000_0001 || G_current_state[76:69] == 8'b0000_0100)) begin
			if(G_pcf_des[38]== 1'b1 || G_pcf_des[39]== 1'b1 ||G_pcf_des[40]== 1'b1||G_pcf_des[41]== 1'b1) begin
				local_integration_cycle[5:0] <= G_pcf_des[7:2];
			end
		end
        /****State_Proc****/
		if(G_current_state[76:69] == 8'b0000_0001) begin 
			if(G_current_state[65] == 1'b1) begin //C0_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
			else if(G_current_state[66] == 1'b1) begin //C1_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
		end
		else if(G_current_state[76:69] == 8'b0000_0100) begin 
			if(G_current_state[50] == 1'b1) begin //C0_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[51] == 1'b1) begin //C1_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
			else if(G_current_state[52] == 1'b1) begin //C2_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
			else if(G_current_state[53] == 1'b1) begin //C3_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
		end
		else if(G_current_state[76:69] == 8'b0000_1000) begin 
			if(G_current_state[42] == 1'b1) begin //C0_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[43] == 1'b1) begin //C1_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[44] == 1'b1) begin //C2_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[45] == 1'b1) begin //C3_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[46] == 1'b1) begin //C4_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
		end
		else if(G_current_state[76:69] == 8'b0001_0000) begin 
			if(G_current_state[36] == 1'b1) begin //C0_S 
				local_integration_cycle[5:0] <= CONF2CORE_sm_static_conf[309:304]; 
			end
			else if(G_current_state[37] == 1'b1) begin //C1_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[38] == 1'b1) begin //C2_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
		end
		else if(G_current_state[76:69] == 8'b0010_0000) begin 
			if(G_current_state[25] == 1'b1) begin //C0_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[27] == 1'b1) begin //C2_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
			else if(G_current_state[32] == 1'b1) begin //C7_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
		end
		else if(G_current_state[76:69] == 8'b0100_0000) begin 
			if(G_current_state[13] == 1'b1) begin //C0_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
			else if(G_current_state[15] == 1'b1) begin //C2_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
			else if(G_current_state[16] == 1'b1) begin //C3_S 
				local_integration_cycle[5:0] <= 6'b0; 
			end
			else if(G_current_state[21] == 1'b1) begin //C8_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
		end
		else if(G_current_state[76:69] == 8'b1000_0000) begin 
			if(G_current_state[3] == 1'b1) begin //C0_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[6] == 1'b1) begin //C3_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
			else if(G_current_state[9] == 1'b1) begin //C6_S 
				local_integration_cycle[5:0] <= 6'b0;
			end
		end
	
    end
end



endmodule