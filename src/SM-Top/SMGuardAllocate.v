/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMGuardAllocate.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

module SMGuardAllocate(
input  wire [348:0]CONF2CORE_sm_static_conf,
input  wire [19:0] G_local_clk,
input  wire        TOC2SC_time_out,
input  wire        G_pcf_des_valid,
input  wire [90:0] G_pcf_des,
output wire        SC2PD_pcf_des_ready,
input  wire [14:0] CD2SC_clique_in,
input  wire [7:0]  stable_cycle_ctr,
input  wire [7:0]  current_top_state,
output wire        GA2SI_guard_pcf_valid,
output wire [3:0]  GA2SI_guard_pcf,
input  wire        SI2GA_guard_pcf_ready,
output wire        GA2SI_guard_timeout,
output wire        GA2SWCSC_guard_pcf_valid,
output wire [1:0]  GA2SWCSC_guard_pcf,
input  wire        SWCSC2GA_guard_pcf_ready,
output wire        GA2SWCSC_guard_timeout,
output wire        GA2SF_guard_pcf_valid,
output wire [1:0]  GA2SF_guard_pcf, 
input  wire        SF2GA_guard_pcf_ready,
output wire        GA2SF_guard_timeout,
output wire        GA2SU_guard_pcf_valid,
output wire [3:0]  GA2SU_guard_pcf,
input  wire        SU2GA_guard_pcf_ready,
output wire        GA2SU_guard_timeout,
output wire [1:0]  GA2SU_guard_conf,
output wire [19:0] GA2STTS_local_clk, 
output wire        GA2STTS_guard_pcf_valid,
output wire [1:0]  GA2STTS_guard_pcf,
input  wire        STTS2GA_guard_pcf_ready,
output wire [7:0]  GA2STTS_guard_stable_cycle_ctr, 
output wire [88:0] GA2STTS_guard_conf,
output wire [5:0]  GA2STTS_guard_clique_in,
output wire [19:0] GA2SYN_local_clk, 
output wire        GA2SYN_guard_pcf_valid,
output wire [1:0]  GA2SYN_guard_pcf,
input  wire        SYN2GA_guard_pcf_ready,
output wire [7:0]  GA2SYN_guard_stable_cycle_ctr,
output wire [88:0] GA2SYN_guard_conf,
output wire [4:0]  GA2SYN_guard_clique_in,
output wire [19:0] GA2STA_local_clk, 
output wire        GA2STA_guard_pcf_valid,
output wire [1:0]  GA2STA_guard_pcf,
input  wire        STA2GA_guard_pcf_ready,
output wire [7:0]  GA2STA_guard_stable_cycle_ctr,
output wire [88:0] GA2STA_guard_conf,
output wire [3:0]  GA2STA_guard_clique_in   

);

//***************************************************
//            PCF Des valid
//***************************************************
assign SC2PD_pcf_des_ready=SI2GA_guard_pcf_ready|SWCSC2GA_guard_pcf_ready|SF2GA_guard_pcf_ready|
                           SU2GA_guard_pcf_ready|STTS2GA_guard_pcf_ready|SYN2GA_guard_pcf_ready|STA2GA_guard_pcf_ready;
						   
//***************************************************
//            SMIntegrate
//***************************************************
assign GA2SI_guard_pcf_valid=G_pcf_des_valid;
assign GA2SI_guard_pcf={G_pcf_des[39:38],G_pcf_des[17:16]};
assign GA2SI_guard_timeout=TOC2SC_time_out;
//***************************************************
//            SMWait4CycleStartCs
//***************************************************
assign GA2SWCSC_guard_pcf_valid=G_pcf_des_valid;
assign GA2SWCSC_guard_pcf=G_pcf_des[17:16];
assign GA2SWCSC_guard_timeout=TOC2SC_time_out;
//***************************************************
//            SMFlood
//***************************************************
assign GA2SF_guard_pcf_valid=G_pcf_des_valid;
assign GA2SF_guard_pcf=G_pcf_des[17:16];
assign GA2SF_guard_timeout=TOC2SC_time_out;
//***************************************************
//            SMUnsync
//***************************************************
assign GA2SU_guard_pcf_valid=G_pcf_des_valid;
assign GA2SU_guard_pcf={G_pcf_des[41:40],G_pcf_des[17:16]};
assign GA2SU_guard_timeout=TOC2SC_time_out;
assign GA2SU_guard_conf=CONF2CORE_sm_static_conf[342:341];
//***************************************************
//            SMTentativeSync
//***************************************************
assign GA2STTS_local_clk=G_local_clk;
assign GA2STTS_guard_pcf_valid=G_pcf_des_valid;
assign GA2STTS_guard_pcf=G_pcf_des[17:16];
assign GA2STTS_guard_stable_cycle_ctr=stable_cycle_ctr[7:0];
assign GA2STTS_guard_conf={CONF2CORE_sm_static_conf[340],CONF2CORE_sm_static_conf[223:216],CONF2CORE_sm_static_conf[99:80],CONF2CORE_sm_static_conf[59:0]};
assign GA2STTS_guard_clique_in={CD2SC_clique_in[10:9],CD2SC_clique_in[3:0]};
//***************************************************
//            SMSync
//***************************************************
assign GA2SYN_local_clk=G_local_clk;
assign GA2SYN_guard_pcf_valid=G_pcf_des_valid;
assign GA2SYN_guard_pcf=G_pcf_des[17:16];
assign GA2SYN_guard_stable_cycle_ctr=stable_cycle_ctr[7:0];
assign GA2SYN_guard_conf={CONF2CORE_sm_static_conf[339],CONF2CORE_sm_static_conf[223:216],CONF2CORE_sm_static_conf[99:80],CONF2CORE_sm_static_conf[59:0]};
assign GA2SYN_guard_clique_in={CD2SC_clique_in[12:11],CD2SC_clique_in[6:4]};
//***************************************************
//            SMStable
//***************************************************
assign GA2STA_local_clk=G_local_clk;
assign GA2STA_guard_pcf_valid=G_pcf_des_valid;
assign GA2STA_guard_pcf=G_pcf_des[17:16];
assign GA2STA_guard_stable_cycle_ctr=stable_cycle_ctr[7:0];
assign GA2STA_guard_conf={CONF2CORE_sm_static_conf[338],CONF2CORE_sm_static_conf[215:208],CONF2CORE_sm_static_conf[99:80],CONF2CORE_sm_static_conf[59:0]};
assign GA2STA_guard_clique_in={CD2SC_clique_in[14:13],CD2SC_clique_in[8:7]};


endmodule  