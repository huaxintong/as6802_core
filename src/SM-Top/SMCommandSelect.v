/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMCommandSelect.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/
`timescale 1ns/1ps
module SMCommandSelect(
output  wire [76:0] G_current_state,
output  wire [68:0] CS2STS_bottom_state,
output  reg         CS2STS_next_top_state_valid,
output  reg [7:0]   CS2STS_next_top_state,
input   wire [7:0]  stable_cycle_ctr,
input   wire [7:0]  current_top_state,
input   wire        SI2CS_cmd_valid,
input   wire [7:0]  SI2CS_cmd_next_top_state,
input   wire        SI2CS_cmd_next_top_state_valid,
input   wire [6:0]  SI2CS_cmd_bottom_state,
input   wire        SWCSC2CS_cmd_valid,
input   wire [7:0]  SWCSC2CS_cmd_next_top_state,
input   wire        SWCSC2CS_cmd_next_top_state_valid,
input   wire [5:0]  SWCSC2CS_cmd_bottom_state,
input   wire        SF2CS_cmd_valid,
input   wire [7:0]  SF2CS_cmd_next_top_state,
input   wire        SF2CS_cmd_next_top_state_valid,
input   wire [7:0]  SF2CS_cmd_bottom_state,
input   wire        SU2CS_cmd_valid,
input   wire [7:0]  SU2CS_cmd_next_top_state,
input   wire        SU2CS_cmd_next_top_state_valid,
input   wire [8:0]  SU2CS_cmd_bottom_state,
input   wire        STTS2CS_cmd_valid,
input   wire [7:0]  STTS2CS_cmd_next_top_state,
input   wire        STTS2CS_cmd_next_top_state_valid,
input   wire [10:0] STTS2CS_cmd_bottom_state,
input   wire        SYN2CS_cmd_valid,
input   wire [7:0]  SYN2CS_cmd_next_top_state,
input   wire        SYN2CS_cmd_next_top_state_valid,
input   wire [11:0] SYN2CS_cmd_bottom_state,
input   wire        STA2CS_cmd_valid,
input   wire [7:0]  STA2CS_cmd_next_top_state,
input   wire        STA2CS_cmd_next_top_state_valid,
input   wire [9:0]  STA2CS_cmd_bottom_state
);



wire sm_bottom_wait_4_cycle_start_state;
assign CS2STS_bottom_state[68:62]=SI2CS_cmd_bottom_state;
assign CS2STS_bottom_state[61:56]=1'b0;
assign CS2STS_bottom_state[55:47]=SU2CS_cmd_bottom_state;
assign CS2STS_bottom_state[46:39]=SF2CS_cmd_bottom_state;
assign CS2STS_bottom_state[38:33]=SWCSC2CS_cmd_bottom_state;
assign CS2STS_bottom_state[32:22]=STTS2CS_cmd_bottom_state;
assign CS2STS_bottom_state[21:10]=SYN2CS_cmd_bottom_state;
assign CS2STS_bottom_state[9:0]=STA2CS_cmd_bottom_state;

///--------------------------------
assign G_current_state[76:69]=current_top_state[7:0];
assign G_current_state[68:0]=CS2STS_bottom_state[68:0];


always@* begin
    case(current_top_state)
        8'b0000_0001:begin  //SM_INTEGRATE
            CS2STS_next_top_state=SI2CS_cmd_next_top_state;
            CS2STS_next_top_state_valid=SI2CS_cmd_next_top_state_valid;
        end 
		8'b0000_0010:begin  //SM_WAIT_4_CYCLE_START
            CS2STS_next_top_state=8'd0;
            CS2STS_next_top_state_valid=1'b0; 
        end 
        8'b0000_0100:begin  //SM_UNSYNC
            CS2STS_next_top_state=SU2CS_cmd_next_top_state;
            CS2STS_next_top_state_valid=SU2CS_cmd_next_top_state_valid;
        end 
        8'b0000_1000:begin  //SM_FLOOD
            CS2STS_next_top_state=SF2CS_cmd_next_top_state;
            CS2STS_next_top_state_valid=SF2CS_cmd_next_top_state_valid;
        end 
		8'b0001_0000:begin  //SM_WAIT_4_CYCLE_START_CS
            CS2STS_next_top_state=SWCSC2CS_cmd_next_top_state;
            CS2STS_next_top_state_valid=SWCSC2CS_cmd_next_top_state_valid;
        end 
        8'b0010_0000:begin  //SM_TENTATIVE_SYNC
            CS2STS_next_top_state=STTS2CS_cmd_next_top_state;
            CS2STS_next_top_state_valid=STTS2CS_cmd_next_top_state_valid;
        end 
        8'b0100_0000:begin  //SM_SYNC
            CS2STS_next_top_state=SYN2CS_cmd_next_top_state;
            CS2STS_next_top_state_valid=SYN2CS_cmd_next_top_state_valid;
        end 
        8'b1000_0000:begin  //SM_STABLE
            CS2STS_next_top_state=STA2CS_cmd_next_top_state;
            CS2STS_next_top_state_valid=STA2CS_cmd_next_top_state_valid;
        end 
        default:begin
             CS2STS_next_top_state=8'd0;
            CS2STS_next_top_state_valid=1'b0;
        end 
    endcase
end 

endmodule