/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMIntegrate.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

module SMIntegrate(
 input wire          SYS2CORE_clk,
 input wire          SYS2CORE_rst_n,
 input wire          GA2SI_guard_pcf_valid,
 input wire [3:0]    GA2SI_guard_pcf,
 output reg          SI2GA_guard_pcf_ready,
 input wire          GA2SI_guard_timeout,
 output reg          SI2CS_cmd_valid,
 output reg  [7:0]   SI2CS_cmd_next_top_state,
 output wire [6:0]   SI2CS_cmd_bottom_state,
 input  wire [7:0]   current_top_state,
 output reg          SI2CS_cmd_next_top_state_valid


);



reg [6:0] SMIntegrate_state;

parameter  INIT_S=7'b000_0001,
           MATCH_S=7'b000_0010,
           COMPLETE_S=7'b000_0100,
           C0_S=7'b000_1000,
           C1_S=7'b001_0000,
           C2_S=7'b010_0000,
           C3_S=7'b100_0000;
           
assign  SI2CS_cmd_bottom_state=SMIntegrate_state; 
        
always@(posedge SYS2CORE_clk or negedge  SYS2CORE_rst_n )begin
    if(~SYS2CORE_rst_n)begin
        SI2CS_cmd_valid<=1'b0;
        SI2CS_cmd_next_top_state<=8'd0;
        SI2CS_cmd_next_top_state_valid<=1'b0;
        SI2GA_guard_pcf_ready<=1'b1;
        SMIntegrate_state<=INIT_S;
    end 
    else begin
        case(SMIntegrate_state)
            INIT_S:begin
                SI2CS_cmd_valid<=1'b0;
                SI2CS_cmd_next_top_state_valid<=1'b0;
                SI2CS_cmd_next_top_state<=8'd0;
                SI2GA_guard_pcf_ready<=1'b1;  
                if(current_top_state==8'b0000_0001)begin
                    SMIntegrate_state<=MATCH_S;
                end 
                else begin
                    SMIntegrate_state<=INIT_S;
                end
                
            end
            MATCH_S:begin
                if(GA2SI_guard_pcf_valid==1'b1&&GA2SI_guard_pcf[1:0]==2'b01)begin  //CA
                    SI2GA_guard_pcf_ready<=1'b0;  
                    SMIntegrate_state<=C1_S;
                end 
                else if(GA2SI_guard_pcf_valid==1'b1&&GA2SI_guard_pcf[1:0]==2'b10&&  //IN
                    GA2SI_guard_pcf[2]==1'b1) begin
                    SI2GA_guard_pcf_ready<=1'b0;  
                    SMIntegrate_state<=C2_S;
                end 
                else if(GA2SI_guard_pcf_valid==1'b1&&GA2SI_guard_pcf[1:0]==2'b10&&  //IN
                    GA2SI_guard_pcf[3]==1'b1) begin
                    SI2GA_guard_pcf_ready<=1'b0;  
                    SMIntegrate_state<=C3_S;
                end 
                else if(GA2SI_guard_timeout==1'b1)begin
                    SI2GA_guard_pcf_ready<=1'b0;  
                    SMIntegrate_state<=C0_S;
                
                end 
                else begin
                    SI2GA_guard_pcf_ready<=1'b1; 
                    SMIntegrate_state<=MATCH_S;
                end     
            end 
            C0_S:begin
                SI2CS_cmd_next_top_state_valid<=1'b1;
                SI2CS_cmd_next_top_state[7:0]<=8'b0000_0100; //SM_UNSYNC
                SI2GA_guard_pcf_ready<=1'b0;
                SMIntegrate_state<=COMPLETE_S;
            end 
            C1_S:begin
                SI2CS_cmd_next_top_state_valid<=1'b1;
                SI2CS_cmd_next_top_state[7:0]<=8'b0001_0000; //SM_WAIT_4_CYCLE_START_CS
                SI2GA_guard_pcf_ready<=1'b0;
                SMIntegrate_state<=COMPLETE_S;
            end
            C2_S:begin
                SI2CS_cmd_next_top_state_valid<=1'b1;
                SI2CS_cmd_next_top_state[7:0]<=8'b0000_0001; //SM_INTEGRATE
                SI2GA_guard_pcf_ready<=1'b0;
                SMIntegrate_state<=COMPLETE_S;
            end
            C3_S:begin
                SI2CS_cmd_next_top_state_valid<=1'b1;
                SI2CS_cmd_next_top_state[7:0]<=8'b0100_0000; //SM_SYNC
                SI2GA_guard_pcf_ready<=1'b0;
                SMIntegrate_state<=COMPLETE_S;
            end
            COMPLETE_S:begin
                SI2CS_cmd_next_top_state_valid<=1'b0;
                SMIntegrate_state<=INIT_S; 
            end 
        endcase
    end 

end 



endmodule