/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SM_Top.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/
`timescale 1ns/1ps
module SM_Top(
    input  wire         SYS2CORE_clk,
    input  wire         SYS2CORE_rst_n,
	input  wire [47:0]  DC2CORE_device_clk,
    output wire [19:0]  SM2LCPM_local_clk,
    input  wire [348:0] CONF2CORE_sm_static_conf,
    input  wire [ 27:0] CONF2CORE_dbg_conf,
    output wire [332:0] CORE2MON_sm_state,
    input  wire         PPM2SM_pcf_rx_cb_wr,
	input  wire [85:0]  PPM2SCM_pcf_rx_cb, 
	output wire         PPM2SM_pcf_rx_cb_ready,
	output wire         SM2OPPM_pcf_tx_cb_wr,
	output wire [63:0]  SM2OPPM_pcf_tx_cb,
	input  wire         SM2OPPM_pcf_tx_cb_ready
);
/*//////////////////////////////////////////////////////////
                    中间变量声明区域
*///////////////////////////////////////////////////////////
//本模块中所有中间变量(wire/reg/parameter)在此集中声明 
wire [19:0] G_local_clk;
wire [5:0]  G_integration_cycle;
wire [90:0] G_pcf_des;
wire G_pcf_des_valid;
wire SC2PD_pcf_des_ready;
wire [76:0] G_current_state;
wire [19:0] SC2LCC_clock_corr;
wire SC2CD_sync_des_valid;
wire [8:0] SC2CD_sync_des;
wire [332:0] SCP_CORE2MON_sm_state;

wire [14:0] CD2SC_clique_in;
wire TOC2SC_time_out;
wire [332:0] SC_CORE2MON_sm_state;

wire LCC2LICC_plus_1;

wire [332:0] TOC_CORE2MON_sm_state;

wire [332:0] CD_CORE2MON_sm_state;


assign SM2LCPM_local_clk[19:0] = G_local_clk[19:0];

assign CORE2MON_sm_state[76:0]  =  SC_CORE2MON_sm_state[76:0];
assign CORE2MON_sm_state[140:77]  =  SM2OPPM_pcf_tx_cb[63:0]; 
assign CORE2MON_sm_state[141:141]  =  SM2OPPM_pcf_tx_cb_wr;
assign CORE2MON_sm_state[232:142]  =  SC_CORE2MON_sm_state[232:142];
assign CORE2MON_sm_state[233:233]  =  SC_CORE2MON_sm_state[233:233];
assign CORE2MON_sm_state[241:234]  =  SC_CORE2MON_sm_state[241:234];
assign CORE2MON_sm_state[249:242] = CD_CORE2MON_sm_state[249:242];
assign CORE2MON_sm_state[257:250] = CD_CORE2MON_sm_state[257:250];
assign CORE2MON_sm_state[265:258] =  CD_CORE2MON_sm_state[265:258];
assign CORE2MON_sm_state[285:266]  =  SCP_CORE2MON_sm_state[285:266];
assign  CORE2MON_sm_state[286:286] =  TOC_CORE2MON_sm_state[286:286];
assign  CORE2MON_sm_state[306:287] =  TOC_CORE2MON_sm_state[306:287];
assign  CORE2MON_sm_state[326:307] =  G_local_clk[19:0];
assign  CORE2MON_sm_state[332:327] =  G_integration_cycle[5:0];



//PermntDelay
SMPermntDelay SMPermntDelay_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.DC2CORE_device_clk(DC2CORE_device_clk),
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf),
.PPM2SM_pcf_rx_cb_wr(PPM2SM_pcf_rx_cb_wr),
.PPM2SCM_pcf_rx_cb(PPM2SCM_pcf_rx_cb),
.PPM2SM_pcf_rx_cb_ready(PPM2SM_pcf_rx_cb_ready),
.G_local_clk(G_local_clk),
.SC2PD_pcf_des_ready(SC2PD_pcf_des_ready), 
.G_pcf_des(G_pcf_des),
.G_pcf_des_valid(G_pcf_des_valid),
.G_current_state(G_current_state)
);

//SyncCompute
SMSyncCompute SMSyncCompute_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),                    
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf), 
.CONF2CORE_dbg_conf(CONF2CORE_dbg_conf),
.G_local_clk(G_local_clk),
.G_integration_cycle(G_integration_cycle), 
.G_pcf_des_valid(G_pcf_des_valid),
.G_pcf_des(G_pcf_des), 
.SC2LCC_clock_corr(SC2LCC_clock_corr),
.SC2CD_sync_des_valid(SC2CD_sync_des_valid),
.SC2CD_sync_des(SC2CD_sync_des),
.CORE2MON_sm_state(SCP_CORE2MON_sm_state)
);

//CliqueDetect
SMCliqueDetect SMCliqueDetect_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),                 
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf),
.CORE2MON_sm_state(CD_CORE2MON_sm_state),
.G_local_clk(G_local_clk),
.G_integration_cycle(G_integration_cycle), 
.SC2CD_sync_des_valid(SC2CD_sync_des_valid),
.SC2CD_sync_des(SC2CD_sync_des),
.CD2SC_clique_in(CD2SC_clique_in),
.G_current_state(G_current_state),
.G_pcf_des(G_pcf_des),
.G_pcf_des_valid(G_pcf_des_valid)
);

//StateControl
SMStateControl SMStateControl_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf),
.G_local_clk(G_local_clk),
.TOC2SC_time_out(TOC2SC_time_out),
.G_pcf_des(G_pcf_des),
.G_pcf_des_valid(G_pcf_des_valid),
.SC2PD_pcf_des_ready(SC2PD_pcf_des_ready),
.G_current_state(G_current_state),
.CORE2MON_sm_state(SC_CORE2MON_sm_state),
.CD2SC_clique_in(CD2SC_clique_in)
);

//LocalClockControl
SMLocalClockControl SMLocalClockControl_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.G_current_state(G_current_state),
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf),
.G_local_clk(G_local_clk),
.SC2LCC_clock_corr(SC2LCC_clock_corr),
.LCC2LICC_plus_1(LCC2LICC_plus_1)
);

//LocalIntegrationCycleControl
SMLocalIntegrationCycleControl  SMLocalIntegrationCycleControl_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.G_current_state(G_current_state),
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf),
.G_pcf_des(G_pcf_des),
.G_pcf_des_valid(G_pcf_des_valid),  
.LCC2LICC_plus_1(LCC2LICC_plus_1),
.G_integration_cycle(G_integration_cycle)
);

//TimeOutControl
SMTimeOutControl SMTimeOutControl_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.G_current_state(G_current_state),
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf),
.TOC2SC_time_out(TOC2SC_time_out),
.CORE2MON_sm_state(TOC_CORE2MON_sm_state)
);

//PcfEventSend
SMPCFEventSend  SMPCFEventSend_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.DC2CORE_device_clk(DC2CORE_device_clk),
.G_current_state(G_current_state),
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf),
.G_integration_cycle(G_integration_cycle),
.G_local_clk(G_local_clk),
.SM2OPPM_pcf_tx_cb_wr(SM2OPPM_pcf_tx_cb_wr),
.SM2OPPM_pcf_tx_cb(SM2OPPM_pcf_tx_cb),
.SM2OPPM_pcf_tx_cb_ready(SM2OPPM_pcf_tx_cb_ready)
);

endmodule