/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMWait4CycleStartCs.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

module SMWait4CycleStartCs(
 input wire         SYS2CORE_clk,
 input wire         SYS2CORE_rst_n,
 input wire         GA2SWCSC_guard_pcf_valid,
 input wire [1:0]   GA2SWCSC_guard_pcf,
 output reg         SWCSC2GA_guard_pcf_ready,
 input wire         GA2SWCSC_guard_timeout,
 output reg         SWCSC2CS_cmd_valid,
 output reg [7:0]   SWCSC2CS_cmd_next_top_state,
 output wire [5:0]  SWCSC2CS_cmd_bottom_state,
 input wire  [7:0]  current_top_state,
 output reg         SWCSC2CS_cmd_next_top_state_valid
);



reg [5:0] SMWait4CycleStartCs_state;

parameter  INIT_S=6'b00_0001,
           MATCH_S=6'b00_0010,
           COMPLETE_S=6'b00_0100,
           C0_S=6'b00_1000,
           C1_S=6'b01_0000,
           C2_S=6'b10_0000;
           
assign  SWCSC2CS_cmd_bottom_state=SMWait4CycleStartCs_state; 
        
always@(posedge SYS2CORE_clk or negedge  SYS2CORE_rst_n )begin
    if(~SYS2CORE_rst_n)begin
        SWCSC2CS_cmd_valid<=1'b0;
        SWCSC2CS_cmd_next_top_state<=8'd0;
        SWCSC2CS_cmd_next_top_state_valid<=1'b0;
        SWCSC2GA_guard_pcf_ready<=1'b1;
        SMWait4CycleStartCs_state<=INIT_S;
    end 
    else begin
        case(SMWait4CycleStartCs_state)
            INIT_S:begin
                SWCSC2CS_cmd_valid<=1'b0;
                SWCSC2CS_cmd_next_top_state_valid<=1'b0;
                SWCSC2CS_cmd_next_top_state<=8'd0;
                SWCSC2GA_guard_pcf_ready<=1'b1;  
                if(current_top_state[7:0]==8'b0001_0000)begin
                    SMWait4CycleStartCs_state<=MATCH_S;
                end 
                else begin
                    SMWait4CycleStartCs_state<=INIT_S;
                end
            end
            MATCH_S:begin
                if(GA2SWCSC_guard_pcf_valid==1'b1&&GA2SWCSC_guard_pcf[1:0]==2'b00)begin  //CS
                    SWCSC2GA_guard_pcf_ready<=1'b0;  
                    SMWait4CycleStartCs_state<=C1_S;
                end 
                else if(GA2SWCSC_guard_pcf_valid==1'b1&&GA2SWCSC_guard_pcf[1:0]==2'b01) begin//CA
                    SWCSC2GA_guard_pcf_ready<=1'b0;  
                    SMWait4CycleStartCs_state<=C2_S;
                end 
                else if(GA2SWCSC_guard_timeout==1'b1)begin
                    SWCSC2GA_guard_pcf_ready<=1'b0;  
                    SMWait4CycleStartCs_state<=C0_S;
                end 
                else begin
                    SWCSC2GA_guard_pcf_ready<=1'b1; 
                    SMWait4CycleStartCs_state<=MATCH_S;
                end     
            end 
            C0_S:begin
                SWCSC2CS_cmd_next_top_state_valid<=1'b1;
                SWCSC2CS_cmd_next_top_state[7:0]<=8'b0010_0000; //SM_TENTATIVE_SYNC
                SWCSC2GA_guard_pcf_ready<=1'b0;
                SMWait4CycleStartCs_state<=COMPLETE_S;
            end 
            C1_S:begin
                SWCSC2CS_cmd_next_top_state_valid<=1'b1;
                SWCSC2CS_cmd_next_top_state[7:0]<=8'b0000_1000; //SM_FLOOD
                SWCSC2GA_guard_pcf_ready<=1'b0;
                SMWait4CycleStartCs_state<=COMPLETE_S;
            end
            C2_S:begin
                SWCSC2CS_cmd_next_top_state_valid<=1'b1;
                SWCSC2CS_cmd_next_top_state[7:0]<=8'b0001_0000; //SM_WAIT_4_CYCLE_START_CS
                SWCSC2GA_guard_pcf_ready<=1'b0;
                SMWait4CycleStartCs_state<=COMPLETE_S;
            end
            COMPLETE_S:begin
                SWCSC2CS_cmd_next_top_state_valid<=1'b0;
                SMWait4CycleStartCs_state<=INIT_S; 
            end 
        endcase
    end 

end 

endmodule