/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMStateControl.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

`timescale 1ns / 1ps
module SMStateControl (
	input  wire          SYS2CORE_clk,
	input  wire          SYS2CORE_rst_n,
	input  wire [348:0]  CONF2CORE_sm_static_conf,
	input  wire [19:0]   G_local_clk,
	input  wire          TOC2SC_time_out,
	input  wire [90:0]   G_pcf_des,
	input  wire          G_pcf_des_valid,
	output wire          SC2PD_pcf_des_ready,
	output wire [76:0]   G_current_state,
	output wire [332:0]  CORE2MON_sm_state,
	input  wire [14:0]   CD2SC_clique_in
);

//***************************************************
//            Intermediate variable Declaration
//***************************************************
//all wire/reg/parameter variable 
//should be declare below here 
//GuardAllocate
wire [7:0]   stable_cycle_ctr;
wire [7:0]   current_top_state;
wire 	     GA2SI_guard_pcf_valid;
wire [3:0]   GA2SI_guard_pcf;
wire         SI2GA_guard_pcf_ready;
wire         GA2SI_guard_timeout;
wire         GA2SWCSC_guard_pcf_valid;
wire [1:0]   GA2SWCSC_guard_pcf;
wire         SWCSC2GA_guard_pcf_ready;
wire         GA2SWCSC_guard_timeout;
wire         GA2SF_guard_pcf_valid;
wire [1:0]   GA2SF_guard_pcf;
wire         SF2GA_guard_pcf_ready;
wire         GA2SF_guard_timeout;
wire         GA2SU_guard_pcf_valid;
wire [3:0]   GA2SU_guard_pcf;
wire         SU2GA_guard_pcf_ready;
wire         GA2SU_guard_timeout;
wire [1:0]   GA2SU_guard_conf;
wire [19:0]  GA2STTS_local_clk;
wire         GA2STTS_guard_pcf_valid;
wire [1:0]   GA2STTS_guard_pcf;
wire         STTS2GA_guard_pcf_ready;
wire [7:0]   GA2STTS_guard_stable_cycle_ctr;
wire [88:0]  GA2STTS_guard_conf;
wire [5:0]   GA2STTS_guard_clique_in;
wire [19:0]  GA2SYN_local_clk;
wire         GA2SYN_guard_pcf_valid;
wire [1:0]   GA2SYN_guard_pcf;
wire         SYN2GA_guard_pcf_ready;
wire [7:0]   GA2SYN_guard_stable_cycle_ctr;
wire [88:0]  GA2SYN_guard_conf;
wire [4:0]   GA2SYN_guard_clique_in;
wire [19:0]  GA2STA_local_clk;
wire         GA2STA_guard_pcf_valid;
wire [1:0]   GA2STA_guard_pcf;
wire         STA2GA_guard_pcf_ready;
wire [7:0]   GA2STA_guard_stable_cycle_ctr;
wire [88:0]  GA2STA_guard_conf;
wire [3:0]   GA2STA_guard_clique_in;

//SMTopState
wire         CS2STS_next_top_state_valid;
wire [7:0]   CS2STS_next_top_state;
wire [68:0]  CS2STS_bottom_state;

//SMIntegrate
wire         SI2CS_cmd_valid;
wire [7:0]   SI2CS_cmd_next_top_state;
wire [6:0]   SI2CS_cmd_bottom_state;
wire         SI2CS_cmd_next_top_state_valid;

//SMWait4CycleStartCs
wire        SWCSC2CS_cmd_valid;
wire [7:0]  SWCSC2CS_cmd_next_top_state;
wire [5:0]  SWCSC2CS_cmd_bottom_state;
wire        SWCSC2CS_cmd_next_top_state_valid;

//SMFlood
wire        SF2CS_cmd_valid;
wire [7:0]  SF2CS_cmd_next_top_state;
wire [7:0]  SF2CS_cmd_bottom_state;
wire        SF2CS_cmd_next_top_state_valid;

//SMUnsync
wire        SU2CS_cmd_valid;
wire [7:0]  SU2CS_cmd_next_top_state;
wire [8:0]  SU2CS_cmd_bottom_state;
wire        SU2CS_cmd_next_top_state_valid;

//SMTentativeSync
wire        STTS2CS_cmd_valid;
wire [7:0]  STTS2CS_cmd_next_top_state;
wire [10:0] STTS2CS_cmd_bottom_state;
wire        STTS2CS_cmd_next_top_state_valid;

//SMSync
wire        SYN2CS_cmd_valid;
wire [7:0]  SYN2CS_cmd_next_top_state;
wire [11:0] SYN2CS_cmd_bottom_state;
wire        SYN2CS_cmd_next_top_state_valid;

//SMStable
wire        STA2CS_cmd_valid;
wire [7:0]  STA2CS_cmd_next_top_state;
wire [9:0]  STA2CS_cmd_bottom_state;
wire        STA2CS_cmd_next_top_state_valid;



assign  CORE2MON_sm_state[76:0] = G_current_state;
assign  CORE2MON_sm_state[141:77] = 65'b0 ;
assign  CORE2MON_sm_state[232:142] = G_pcf_des[90:0];
assign  CORE2MON_sm_state[233:233] = G_pcf_des_valid;
assign  CORE2MON_sm_state[241:234] = stable_cycle_ctr;
assign  CORE2MON_sm_state[332:242] = 91'b0;

//***************************************************
//            GuardAllocate
//***************************************************
SMGuardAllocate SMGuardAllocate_inst(
.CONF2CORE_sm_static_conf(CONF2CORE_sm_static_conf), 
.G_local_clk(G_local_clk), 
.TOC2SC_time_out(TOC2SC_time_out),
.G_pcf_des_valid(G_pcf_des_valid),
.G_pcf_des(G_pcf_des),
.SC2PD_pcf_des_ready(SC2PD_pcf_des_ready),
.CD2SC_clique_in(CD2SC_clique_in),
.stable_cycle_ctr(stable_cycle_ctr),
.current_top_state(current_top_state),
.GA2SI_guard_pcf_valid(GA2SI_guard_pcf_valid),
.GA2SI_guard_pcf(GA2SI_guard_pcf),
.SI2GA_guard_pcf_ready(SI2GA_guard_pcf_ready),
.GA2SI_guard_timeout(GA2SI_guard_timeout),
.GA2SWCSC_guard_pcf_valid(GA2SWCSC_guard_pcf_valid),
.GA2SWCSC_guard_pcf(GA2SWCSC_guard_pcf),
.SWCSC2GA_guard_pcf_ready(SWCSC2GA_guard_pcf_ready),
.GA2SWCSC_guard_timeout(GA2SWCSC_guard_timeout),
.GA2SF_guard_pcf_valid(GA2SF_guard_pcf_valid),
.GA2SF_guard_pcf(GA2SF_guard_pcf), 
.SF2GA_guard_pcf_ready(SF2GA_guard_pcf_ready),
.GA2SF_guard_timeout(GA2SF_guard_timeout),
.GA2SU_guard_pcf_valid(GA2SU_guard_pcf_valid),
.GA2SU_guard_pcf(GA2SU_guard_pcf),
.SU2GA_guard_pcf_ready(SU2GA_guard_pcf_ready),
.GA2SU_guard_timeout(GA2SU_guard_timeout),
.GA2SU_guard_conf(GA2SU_guard_conf),
.GA2STTS_local_clk(GA2STTS_local_clk), 
.GA2STTS_guard_pcf_valid(GA2STTS_guard_pcf_valid),
.GA2STTS_guard_pcf(GA2STTS_guard_pcf),
.STTS2GA_guard_pcf_ready(STTS2GA_guard_pcf_ready),
.GA2STTS_guard_stable_cycle_ctr(GA2STTS_guard_stable_cycle_ctr), 
.GA2STTS_guard_conf(GA2STTS_guard_conf),
.GA2STTS_guard_clique_in(GA2STTS_guard_clique_in),
.GA2SYN_local_clk(GA2SYN_local_clk), 
.GA2SYN_guard_pcf_valid(GA2SYN_guard_pcf_valid),
.GA2SYN_guard_pcf(GA2SYN_guard_pcf),
.SYN2GA_guard_pcf_ready(SYN2GA_guard_pcf_ready),
.GA2SYN_guard_stable_cycle_ctr(GA2SYN_guard_stable_cycle_ctr),
.GA2SYN_guard_conf(GA2SYN_guard_conf),
.GA2SYN_guard_clique_in(GA2SYN_guard_clique_in),
.GA2STA_local_clk(GA2STA_local_clk), 
.GA2STA_guard_pcf_valid(GA2STA_guard_pcf_valid),
.GA2STA_guard_pcf(GA2STA_guard_pcf),
.STA2GA_guard_pcf_ready(STA2GA_guard_pcf_ready),
.GA2STA_guard_stable_cycle_ctr(GA2STA_guard_stable_cycle_ctr),
.GA2STA_guard_conf(GA2STA_guard_conf),
.GA2STA_guard_clique_in(GA2STA_guard_clique_in)
);

//***************************************************
//            SMTopState
//***************************************************
SMTopState SMTopState_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.stable_cycle_ctr(stable_cycle_ctr),
.current_top_state(current_top_state),
.CS2STS_next_top_state_valid(CS2STS_next_top_state_valid),
.CS2STS_next_top_state(CS2STS_next_top_state),
.CS2STS_bottom_state(CS2STS_bottom_state)
);

//***************************************************
//            SMIntegrate
//***************************************************
SMIntegrate SMIntegrate_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.GA2SI_guard_pcf_valid(GA2SI_guard_pcf_valid),
.GA2SI_guard_pcf(GA2SI_guard_pcf),
.SI2GA_guard_pcf_ready(SI2GA_guard_pcf_ready),
.GA2SI_guard_timeout(GA2SI_guard_timeout),
.SI2CS_cmd_valid(SI2CS_cmd_valid),
.SI2CS_cmd_next_top_state(SI2CS_cmd_next_top_state),
.SI2CS_cmd_bottom_state(SI2CS_cmd_bottom_state),
.current_top_state(current_top_state),
.SI2CS_cmd_next_top_state_valid(SI2CS_cmd_next_top_state_valid)
);


//***************************************************
//            SMWait4CycleStartCs
//***************************************************
SMWait4CycleStartCs SMWait4CycleStartCs_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.GA2SWCSC_guard_pcf_valid(GA2SWCSC_guard_pcf_valid),
.GA2SWCSC_guard_pcf(GA2SWCSC_guard_pcf),
.SWCSC2GA_guard_pcf_ready(SWCSC2GA_guard_pcf_ready),
.GA2SWCSC_guard_timeout(GA2SWCSC_guard_timeout),
.SWCSC2CS_cmd_valid(SWCSC2CS_cmd_valid),
.SWCSC2CS_cmd_next_top_state(SWCSC2CS_cmd_next_top_state),
.SWCSC2CS_cmd_bottom_state(SWCSC2CS_cmd_bottom_state),
.current_top_state(current_top_state),
.SWCSC2CS_cmd_next_top_state_valid(SWCSC2CS_cmd_next_top_state_valid)
);

//***************************************************
//            SMFlood
//***************************************************
SMFlood SMFlood_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.GA2SF_guard_pcf_valid(GA2SF_guard_pcf_valid),
.GA2SF_guard_pcf(GA2SF_guard_pcf),
.SF2GA_guard_pcf_ready(SF2GA_guard_pcf_ready),
.GA2SF_guard_timeout(GA2SF_guard_timeout),
.SF2CS_cmd_valid(SF2CS_cmd_valid),
.SF2CS_cmd_next_top_state(SF2CS_cmd_next_top_state),
.SF2CS_cmd_bottom_state(SF2CS_cmd_bottom_state),
.current_top_state(current_top_state),
.SF2CS_cmd_next_top_state_valid(SF2CS_cmd_next_top_state_valid) 
);

//***************************************************
//            SMUnsync
//***************************************************
SMUnsync SMUnsync_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.GA2SU_guard_pcf_valid(GA2SU_guard_pcf_valid),
.GA2SU_guard_pcf(GA2SU_guard_pcf),
.SU2GA_guard_pcf_ready(SU2GA_guard_pcf_ready),
.GA2SU_guard_timeout(GA2SU_guard_timeout),
.GA2SU_guard_conf(GA2SU_guard_conf),
.SU2CS_cmd_valid(SU2CS_cmd_valid),
.SU2CS_cmd_next_top_state(SU2CS_cmd_next_top_state),
.SU2CS_cmd_bottom_state(SU2CS_cmd_bottom_state),
.current_top_state(current_top_state),
.SU2CS_cmd_next_top_state_valid(SU2CS_cmd_next_top_state_valid) 
);

//***************************************************
//            SMTentativeSync
//***************************************************
SMTentativeSync SMTentativeSync_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.GA2STTS_local_clk(GA2STTS_local_clk),
.GA2STTS_guard_pcf_valid(GA2STTS_guard_pcf_valid),
.GA2STTS_guard_pcf(GA2STTS_guard_pcf),
.STTS2GA_guard_pcf_ready(STTS2GA_guard_pcf_ready),
.GA2STTS_guard_stable_cycle_ctr(GA2STTS_guard_stable_cycle_ctr),
.GA2STTS_guard_conf(GA2STTS_guard_conf),
.GA2STTS_guard_clique_in(GA2STTS_guard_clique_in),
.STTS2CS_cmd_valid(STTS2CS_cmd_valid),
.STTS2CS_cmd_next_top_state(STTS2CS_cmd_next_top_state),
.STTS2CS_cmd_bottom_state(STTS2CS_cmd_bottom_state),
.current_top_state(current_top_state),
.STTS2CS_cmd_next_top_state_valid(STTS2CS_cmd_next_top_state_valid)
);


//***************************************************
//            SMSync
//***************************************************
SMSync SMSync_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.GA2SYN_local_clk(GA2SYN_local_clk),
.GA2SYN_guard_pcf_valid(GA2SYN_guard_pcf_valid),
.GA2SYN_guard_pcf(GA2SYN_guard_pcf),
.SYN2GA_guard_pcf_ready(SYN2GA_guard_pcf_ready),
.GA2SYN_guard_stable_cycle_ctr(GA2SYN_guard_stable_cycle_ctr),
.GA2SYN_guard_conf(GA2SYN_guard_conf),
.GA2SYN_guard_clique_in(GA2SYN_guard_clique_in),
.SYN2CS_cmd_valid(SYN2CS_cmd_valid),
.SYN2CS_cmd_next_top_state(SYN2CS_cmd_next_top_state),
.SYN2CS_cmd_bottom_state(SYN2CS_cmd_bottom_state),
.current_top_state(current_top_state),
.SYN2CS_cmd_next_top_state_valid(SYN2CS_cmd_next_top_state_valid)
);

//***************************************************
//            SMStable
//***************************************************
SMStable SMStable_inst(
.SYS2CORE_clk(SYS2CORE_clk),
.SYS2CORE_rst_n(SYS2CORE_rst_n),
.GA2STA_local_clk(GA2STA_local_clk),
.GA2STA_guard_pcf_valid(GA2STA_guard_pcf_valid),
.GA2STA_guard_pcf(GA2STA_guard_pcf),
.STA2GA_guard_pcf_ready(STA2GA_guard_pcf_ready),
.GA2STA_guard_stable_cycle_ctr(GA2STA_guard_stable_cycle_ctr),
.GA2STA_guard_conf(GA2STA_guard_conf),
.GA2STA_guard_clique_in(GA2STA_guard_clique_in),
.STA2CS_cmd_valid(STA2CS_cmd_valid),
.STA2CS_cmd_next_top_state(STA2CS_cmd_next_top_state),
.STA2CS_cmd_bottom_state(STA2CS_cmd_bottom_state),
.current_top_state(current_top_state),
.STA2CS_cmd_next_top_state_valid(STA2CS_cmd_next_top_state_valid)
);

//***************************************************
//            CommandSelect
//***************************************************
SMCommandSelect SMCommandSelect_inst(
.G_current_state(G_current_state),
.CS2STS_bottom_state(CS2STS_bottom_state),
.CS2STS_next_top_state_valid(CS2STS_next_top_state_valid),
.CS2STS_next_top_state(CS2STS_next_top_state),
.stable_cycle_ctr(stable_cycle_ctr),
.current_top_state(current_top_state),
.SI2CS_cmd_valid(SI2CS_cmd_valid),
.SI2CS_cmd_next_top_state(SI2CS_cmd_next_top_state),
.SI2CS_cmd_next_top_state_valid(SI2CS_cmd_next_top_state_valid),
.SI2CS_cmd_bottom_state(SI2CS_cmd_bottom_state),
.SWCSC2CS_cmd_valid(SWCSC2CS_cmd_valid),
.SWCSC2CS_cmd_next_top_state(SWCSC2CS_cmd_next_top_state),
.SWCSC2CS_cmd_next_top_state_valid(SWCSC2CS_cmd_next_top_state_valid),
.SWCSC2CS_cmd_bottom_state(SWCSC2CS_cmd_bottom_state),
.SF2CS_cmd_valid(SF2CS_cmd_valid),
.SF2CS_cmd_next_top_state(SF2CS_cmd_next_top_state),
.SF2CS_cmd_next_top_state_valid(SF2CS_cmd_next_top_state_valid),
.SF2CS_cmd_bottom_state(SF2CS_cmd_bottom_state),
.SU2CS_cmd_valid(SU2CS_cmd_valid),
.SU2CS_cmd_next_top_state(SU2CS_cmd_next_top_state),
.SU2CS_cmd_next_top_state_valid(SU2CS_cmd_next_top_state_valid),
.SU2CS_cmd_bottom_state(SU2CS_cmd_bottom_state),
.STTS2CS_cmd_valid(STTS2CS_cmd_valid),
.STTS2CS_cmd_next_top_state(STTS2CS_cmd_next_top_state),
.STTS2CS_cmd_next_top_state_valid(STTS2CS_cmd_next_top_state_valid),
.STTS2CS_cmd_bottom_state(STTS2CS_cmd_bottom_state),
.SYN2CS_cmd_valid(SYN2CS_cmd_valid),
.SYN2CS_cmd_next_top_state(SYN2CS_cmd_next_top_state),
.SYN2CS_cmd_next_top_state_valid(SYN2CS_cmd_next_top_state_valid),
.SYN2CS_cmd_bottom_state(SYN2CS_cmd_bottom_state),
.STA2CS_cmd_valid(STA2CS_cmd_valid),
.STA2CS_cmd_next_top_state(STA2CS_cmd_next_top_state),
.STA2CS_cmd_next_top_state_valid(STA2CS_cmd_next_top_state_valid),
.STA2CS_cmd_bottom_state(STA2CS_cmd_bottom_state)
);

endmodule