/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMPCFEventSend.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

`timescale 1ns/1ps
module SMPCFEventSend(
    input  wire         SYS2CORE_clk,
    input  wire         SYS2CORE_rst_n,
	input  wire [47:0]  DC2CORE_device_clk,
    input  wire [76:0]  G_current_state,
    input  wire [348:0] CONF2CORE_sm_static_conf,
	input  wire [5:0]   G_integration_cycle,
	input  wire [19:0]  G_local_clk,
	output reg          SM2OPPM_pcf_tx_cb_wr,
	output reg  [63:0]  SM2OPPM_pcf_tx_cb,
	input  wire         SM2OPPM_pcf_tx_cb_ready
);
//***************************************************
//            Intermediate variable Declaration
//***************************************************
//all wire/reg/parameter variable 
//should be declare below here 




always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
    if(~SYS2CORE_rst_n) begin
		SM2OPPM_pcf_tx_cb_wr <= 1'b0;
		SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
    end
    else begin
		if(SM2OPPM_pcf_tx_cb_ready==1'b1) begin
			if(G_current_state[76:69] == 8'b0000_0001) begin 
				if(G_current_state[65] == 1'b1) begin //C0_S 
					SM2OPPM_pcf_tx_cb[63:16] <= DC2CORE_device_clk-1;//
					SM2OPPM_pcf_tx_cb[15:14] <= 2'b00;//CS
					SM2OPPM_pcf_tx_cb[5:0] <= G_integration_cycle;//
					SM2OPPM_pcf_tx_cb[13:6] <= CONF2CORE_sm_static_conf[337:330];//
					SM2OPPM_pcf_tx_cb_wr <= 1'b1;
				end
				else begin
					SM2OPPM_pcf_tx_cb_wr <= 1'b0;
					SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
				end
			end
			else if(G_current_state[76:69] == 8'b0000_0100) begin 
				if(G_current_state[50] == 1'b1) begin //C0_S 
					SM2OPPM_pcf_tx_cb[63:16] <= DC2CORE_device_clk-1;//
					SM2OPPM_pcf_tx_cb[15:14] <= 2'b00;//CS
					SM2OPPM_pcf_tx_cb[5:0] <= G_integration_cycle;//
					SM2OPPM_pcf_tx_cb[13:6] <= CONF2CORE_sm_static_conf[337:330];//
					SM2OPPM_pcf_tx_cb_wr <= 1'b1;
				end
				else begin
					SM2OPPM_pcf_tx_cb_wr <= 1'b0;
					SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
				end
			end
			else if(G_current_state[76:69] == 8'b0000_1000) begin 
				if(G_current_state[42] == 1'b1) begin //C0_S 
					SM2OPPM_pcf_tx_cb[63:16] <= DC2CORE_device_clk-1;//
					SM2OPPM_pcf_tx_cb[15:14] <= 2'b01;//CA
					SM2OPPM_pcf_tx_cb[5:0] <= G_integration_cycle;//
					SM2OPPM_pcf_tx_cb[13:6] <= CONF2CORE_sm_static_conf[337:330];//
					SM2OPPM_pcf_tx_cb_wr <= 1'b1;
				end
				else begin
					SM2OPPM_pcf_tx_cb_wr <= 1'b0;
					SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
				end
			end
			else if(G_current_state[76:69] == 8'b0001_0000) begin 
				if(G_current_state[36] == 1'b1) begin //C0_S
					SM2OPPM_pcf_tx_cb[63:16] <= DC2CORE_device_clk-1;//
					SM2OPPM_pcf_tx_cb[15:14] <= 2'b10;//IN
					SM2OPPM_pcf_tx_cb[5:0] <= G_integration_cycle;//
					SM2OPPM_pcf_tx_cb[13:6] <= CONF2CORE_sm_static_conf[337:330];//
					SM2OPPM_pcf_tx_cb_wr <= 1'b1;
				end
				else begin
					SM2OPPM_pcf_tx_cb_wr <= 1'b0;
					SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
				end
			end
			else if(G_current_state[76:69] == 8'b0010_0000) begin 
				if(G_current_state[26] == 1'b1) begin //C1_S 
					SM2OPPM_pcf_tx_cb[63:16] <= DC2CORE_device_clk-1;//
					SM2OPPM_pcf_tx_cb[15:14] <= 2'b10;//IN
					SM2OPPM_pcf_tx_cb[5:0] <= G_integration_cycle;//
					SM2OPPM_pcf_tx_cb[13:6] <= CONF2CORE_sm_static_conf[337:330];//
					SM2OPPM_pcf_tx_cb_wr <= 1'b1;
				end
				else begin
					SM2OPPM_pcf_tx_cb_wr <= 1'b0;
					SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
				end
			end
			else if(G_current_state[76:69] == 8'b0100_0000) begin 
				if(G_current_state[14] == 1'b1) begin //C1_S 
					SM2OPPM_pcf_tx_cb[15:14] <= 2'b10;//IN
					SM2OPPM_pcf_tx_cb[63:16] <= DC2CORE_device_clk-1;//
					SM2OPPM_pcf_tx_cb[5:0] <= G_integration_cycle;//
					SM2OPPM_pcf_tx_cb[13:6] <= CONF2CORE_sm_static_conf[337:330];//
					SM2OPPM_pcf_tx_cb_wr <= 1'b1;
				end
				else begin
					SM2OPPM_pcf_tx_cb_wr <= 1'b0;
					SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
				end
			end
			else if(G_current_state[76:69] == 8'b1000_0000) begin 
				if(G_current_state[4] == 1'b1) begin //C1_S 
					SM2OPPM_pcf_tx_cb[15:14] <= 2'b10;//IN
					SM2OPPM_pcf_tx_cb[63:16] <= DC2CORE_device_clk-1;//
					SM2OPPM_pcf_tx_cb[5:0] <= G_integration_cycle;//
					SM2OPPM_pcf_tx_cb[13:6] <= CONF2CORE_sm_static_conf[337:330];//
					SM2OPPM_pcf_tx_cb_wr <= 1'b1;
				end
				else begin
					SM2OPPM_pcf_tx_cb_wr <= 1'b0;
					SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
				end
			end
			else begin
				SM2OPPM_pcf_tx_cb_wr <= 1'b0;
				SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
			end
		end
		else begin
			SM2OPPM_pcf_tx_cb_wr <= 1'b0;
			SM2OPPM_pcf_tx_cb[63:0] <= 64'b0;
		end
    end
end

endmodule