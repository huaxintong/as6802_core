/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */

/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMSync.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

module SMSync(
 input wire         SYS2CORE_clk,
 input wire         SYS2CORE_rst_n,
 input wire [19:0]  GA2SYN_local_clk,
 input wire         GA2SYN_guard_pcf_valid,
 input wire [1:0]   GA2SYN_guard_pcf,
 output reg         SYN2GA_guard_pcf_ready,
 input wire [7:0]   GA2SYN_guard_stable_cycle_ctr,
 input wire [88:0]  GA2SYN_guard_conf,
 input wire [4:0]   GA2SYN_guard_clique_in,
 output reg         SYN2CS_cmd_valid,
 output reg [7:0]   SYN2CS_cmd_next_top_state,
 output wire [11:0] SYN2CS_cmd_bottom_state,
 input wire  [7:0]  current_top_state,
 output reg         SYN2CS_cmd_next_top_state_valid


);



reg [11:0] SMSync_state;

parameter  INIT_S=12'b0000_0000_0001,
           MATCH_S=12'b0000_0000_0010,
           COMPLETE_S=12'b0000_0000_0100,
           C0_S=12'b0000_0000_1000,
           C1_S=12'b0000_0001_0000,
           C2_S=12'b0000_0010_0000,
		   C3_S=12'b0000_0100_0000,
		   C4_S=12'b0000_1000_0000,
		   C5_S=12'b0001_0000_0000,
		   C6_S=12'b0010_0000_0000,
		   C7_S=12'b0100_0000_0000,
           C8_S=12'b1000_0000_0000;
           
assign  SYN2CS_cmd_bottom_state=SMSync_state; 
        
always@(posedge SYS2CORE_clk or negedge  SYS2CORE_rst_n )begin
    if(~SYS2CORE_rst_n)begin
        SYN2CS_cmd_valid<=1'b0;
        SYN2CS_cmd_next_top_state<=8'd0;
        SYN2CS_cmd_next_top_state_valid<=1'b0;
        SYN2GA_guard_pcf_ready<=1'b1;
        SMSync_state<=INIT_S;
    end 
    else begin
        case(SMSync_state)
            INIT_S:begin
                SYN2CS_cmd_valid<=1'b0;
                SYN2CS_cmd_next_top_state_valid<=1'b0;
                SYN2CS_cmd_next_top_state<=8'd0;
                SYN2GA_guard_pcf_ready<=1'b1;  
                if(current_top_state==8'b0100_0000)begin
                    SMSync_state<=MATCH_S;
                end 
                else begin
                    SMSync_state<=INIT_S;
                end
                
            end
            MATCH_S:begin
                if(GA2SYN_guard_pcf_valid==1'b1&&GA2SYN_guard_pcf[1:0]==2'b01)begin  //CA
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C8_S;
                end 
                else if(GA2SYN_local_clk==GA2SYN_guard_conf[39:20]&&GA2SYN_guard_clique_in[3]==1'b1) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C0_S;
                end 
                else if(GA2SYN_local_clk==GA2SYN_guard_conf[79:60]&&GA2SYN_guard_clique_in[4]==1'b1) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C1_S;
                end 
                else if(GA2SYN_local_clk==GA2SYN_guard_conf[59:40]&&GA2SYN_guard_clique_in[0]==1'b1) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C2_S;
                end 
				else if(GA2SYN_local_clk==GA2SYN_guard_conf[59:40]&&GA2SYN_guard_clique_in[1]==1'b1) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C3_S;
                end 
				else if(GA2SYN_local_clk==GA2SYN_guard_conf[59:40]&&GA2SYN_guard_clique_in[2]==1'b1&&
						GA2SYN_guard_conf[88]==1'b1&&GA2SYN_guard_stable_cycle_ctr[7:0]<GA2SYN_guard_conf[87:80]) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C5_S;
                end 
				else if(GA2SYN_local_clk==GA2SYN_guard_conf[59:40]&&GA2SYN_guard_clique_in[2]==1'b1&&
						GA2SYN_guard_conf[88]==1'b1&&GA2SYN_guard_stable_cycle_ctr[7:0]>=GA2SYN_guard_conf[87:80]) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C6_S;
                end 
				else if(GA2SYN_local_clk==GA2SYN_guard_conf[59:40]&&GA2SYN_guard_clique_in[2]==1'b1) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C4_S;
                end 
				else if((GA2SYN_local_clk+1'b1)==GA2SYN_guard_conf[19:0]) begin
                    SYN2GA_guard_pcf_ready<=1'b0;  
                    SMSync_state<=C7_S;
				end
                else begin
                    SYN2GA_guard_pcf_ready<=1'b1; 
                    SMSync_state<=MATCH_S;
                end     
            end 
            C0_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0000_0100; //SM_UNSYNC
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end 
            C1_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0100_0000; //SM_SYNC
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
            C2_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0000_0100; //SM_UNSYNC
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
            C3_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0000_0001; //SM_INTEGRATE
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
			C4_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0100_0000; //SM_SYNC
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
			C5_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0100_0000; //SM_SYNC
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
			C6_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b1000_0000; //SM_STABLE
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
			C7_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0100_0000; //SM_SYNC
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
			C8_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b1;
                SYN2CS_cmd_next_top_state[7:0]<=8'b0001_0000; //SM_WAIT_4_CYCLE_START_CS
                SYN2GA_guard_pcf_ready<=1'b0;
                SMSync_state<=COMPLETE_S;
            end
            COMPLETE_S:begin
                SYN2CS_cmd_next_top_state_valid<=1'b0;
                SMSync_state<=INIT_S; 
            end 
        endcase
    end 

end 



endmodule