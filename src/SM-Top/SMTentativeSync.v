/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMTentativeSync.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

module SMTentativeSync(
 input wire         SYS2CORE_clk,
 input wire         SYS2CORE_rst_n,
 input wire [19:0]  GA2STTS_local_clk,
 input wire         GA2STTS_guard_pcf_valid,
 input wire [1:0]   GA2STTS_guard_pcf,
 output reg         STTS2GA_guard_pcf_ready,
 input wire [7:0]   GA2STTS_guard_stable_cycle_ctr,
 input wire [88:0]  GA2STTS_guard_conf,
 input wire [5:0]   GA2STTS_guard_clique_in,
 output reg         STTS2CS_cmd_valid,
 output reg [7:0]   STTS2CS_cmd_next_top_state,
 output wire [10:0] STTS2CS_cmd_bottom_state,
 input wire  [7:0]  current_top_state,
 output reg         STTS2CS_cmd_next_top_state_valid


);



reg [10:0] SMTentativeSync_state;

parameter  INIT_S=11'b000_0000_0001,
           MATCH_S=11'b000_0000_0010,
           COMPLETE_S=11'b000_0000_0100,
           C0_S=11'b000_0000_1000,
           C1_S=11'b000_0001_0000,
           C2_S=11'b000_0010_0000,
		   C3_S=11'b000_0100_0000,
		   C4_S=11'b000_1000_0000,
		   C5_S=11'b001_0000_0000,
		   C6_S=11'b010_0000_0000,
           C7_S=11'b100_0000_0000;
           
assign  STTS2CS_cmd_bottom_state=SMTentativeSync_state; 
        
always@(posedge SYS2CORE_clk or negedge  SYS2CORE_rst_n )begin
    if(~SYS2CORE_rst_n)begin
        STTS2CS_cmd_valid<=1'b0;
        STTS2CS_cmd_next_top_state<=8'd0;
        STTS2CS_cmd_next_top_state_valid<=1'b0;
        STTS2GA_guard_pcf_ready<=1'b1;
        SMTentativeSync_state<=INIT_S;
    end 
    else begin
        case(SMTentativeSync_state)
            INIT_S:begin
                STTS2CS_cmd_valid<=1'b0;
                STTS2CS_cmd_next_top_state_valid<=1'b0;
                STTS2CS_cmd_next_top_state<=8'd0;
                STTS2GA_guard_pcf_ready<=1'b1;  
                if(current_top_state==8'b0010_0000)begin
                    SMTentativeSync_state<=MATCH_S;
                end 
                else begin
                    SMTentativeSync_state<=INIT_S;
                end
                
            end
            MATCH_S:begin
                if(GA2STTS_guard_pcf_valid==1'b1&&GA2STTS_guard_pcf[1:0]==2'b01)begin  //CA
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C7_S;
                end 
                else if(GA2STTS_local_clk==GA2STTS_guard_conf[39:20]&&GA2STTS_guard_clique_in[4]==1'b1) begin
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C0_S;
                end 
                else if(GA2STTS_local_clk==GA2STTS_guard_conf[79:60]&&GA2STTS_guard_clique_in[5]==1'b1) begin
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C1_S;
                end 
                else if(GA2STTS_local_clk==GA2STTS_guard_conf[59:40]&&GA2STTS_guard_clique_in[0]==1'b1) begin
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C2_S;
                end 
				else if(GA2STTS_local_clk==GA2STTS_guard_conf[59:40]&&GA2STTS_guard_clique_in[1]==1'b1) begin
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C3_S;
                end 
				else if(GA2STTS_local_clk==GA2STTS_guard_conf[59:40]&&GA2STTS_guard_clique_in[2]==1'b1) begin
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C4_S;
                end 
				else if(GA2STTS_local_clk==GA2STTS_guard_conf[59:40]&&GA2STTS_guard_clique_in[3]==1'b1&&GA2STTS_guard_conf[88]==1'b1&&
						GA2STTS_guard_stable_cycle_ctr>=GA2STTS_guard_conf[87:80]) begin
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C5_S;
                end 
				else if((GA2STTS_local_clk+1'b1)==GA2STTS_guard_conf[19:0]) begin
                    STTS2GA_guard_pcf_ready<=1'b0;  
                    SMTentativeSync_state<=C6_S;
				end
                else begin
                    STTS2GA_guard_pcf_ready<=1'b1; 
                    SMTentativeSync_state<=MATCH_S;
                end     
            end 
            C0_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b0000_0100; //SM_UNSYNC
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end 
            C1_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b0010_0000; //SM_TENTATIVE_SYNC
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end
            C2_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b0000_0100; //SM_UNSYNC
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end
            C3_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b0010_0000; //SM_TENTATIVE_SYNC
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end
			C4_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b0100_0000; //SM_SYNC
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end
			C5_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b1000_0000; //SM_STABLE
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end
			C6_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b0010_0000; //SM_TENTATIVE_SYNC
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end
			C7_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b1;
                STTS2CS_cmd_next_top_state[7:0]<=8'b0001_0000; //SM_WAIT_4_CYCLE_START_CS
                STTS2GA_guard_pcf_ready<=1'b0;
                SMTentativeSync_state<=COMPLETE_S;
            end
            COMPLETE_S:begin
                STTS2CS_cmd_next_top_state_valid<=1'b0;
                SMTentativeSync_state<=INIT_S; 
            end 
        endcase
    end 

end 



endmodule