/*
 *Copyright (c) [2019-2020]  C2comm, Inc.  All rights reserved.
 *
 *This program is free software; you can redistribute it and/or
 *modify it under the terms of the GNU General Public License
 *as published by the Free Software Foundation; either version 2
 *of the License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program; If not, see <https://www.gnu.org/licenses/>
 */
/*
 *Vendor: C2comm
 *Version: 1.0
 *Filename: SMTopState.v
 *Target Device: Altera
 *Author : sunxiaotian@c2comm.cn
*/

`timescale 1ns / 1ps
module SMTopState (
	input  wire          SYS2CORE_clk,
	input  wire          SYS2CORE_rst_n,
	output reg [7:0]     stable_cycle_ctr,
	output reg  [7:0]    current_top_state,
	input  wire          CS2STS_next_top_state_valid,
	input  wire [7:0]    CS2STS_next_top_state,
	input  wire [68:0]   CS2STS_bottom_state
);


//***************************************************
//            StableCycleCtrMgmt
//***************************************************
always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
	if(~SYS2CORE_rst_n) begin
		stable_cycle_ctr[7:0] <= 8'b0;//SM_INTEGRATE
	end
	else begin
		if((CS2STS_bottom_state[25] == 1'b1)||
			CS2STS_bottom_state[27] == 1'b1 || CS2STS_bottom_state[29] == 1'b1 ||
			CS2STS_bottom_state[30] == 1'b1 || CS2STS_bottom_state[32] == 1'b1  ) begin //[32:22]->SM_TENTATIVE_SYNC
			stable_cycle_ctr[7:0] <= 8'b0;
		end
		else if((CS2STS_bottom_state[13] == 1'b1)||
			CS2STS_bottom_state[15] == 1'b1 || CS2STS_bottom_state[16] == 1'b1 ||
			CS2STS_bottom_state[19] == 1'b1 || CS2STS_bottom_state[21] == 1'b1  ) begin //[21:10]->SM_SYNC
			stable_cycle_ctr[7:0] <= 8'b0;
		end
		else if((CS2STS_bottom_state[3] == 1'b1)||
			CS2STS_bottom_state[6] == 1'b1 || CS2STS_bottom_state[7] == 1'b1 ||
			CS2STS_bottom_state[9] == 1'b1 || CS2STS_bottom_state[21] == 1'b1  ) begin //[9:0]->SM_STABLE
			stable_cycle_ctr[7:0] <= 8'b0;
		end
		else if(CS2STS_bottom_state[28] == 1'b1) begin
			stable_cycle_ctr[7:0] <= stable_cycle_ctr[7:0] +1'b1;
		end
		else if(CS2STS_bottom_state[17] == 1'b1||CS2STS_bottom_state[18] == 1'b1) begin
			stable_cycle_ctr[7:0] <= stable_cycle_ctr[7:0] +1'b1;
		end
		else if(CS2STS_bottom_state[5] == 1'b1) begin
			stable_cycle_ctr[7:0] <= stable_cycle_ctr[7:0] +1'b1;
		end

		
	end
end


//***************************************************
//            TopStateMgmt
//***************************************************

always @(posedge SYS2CORE_clk or negedge SYS2CORE_rst_n) begin
	if(~SYS2CORE_rst_n) begin
		current_top_state[7:0] <= 8'b0000_0001;//SM_INTEGRATE
	end
	else begin
		if(CS2STS_next_top_state_valid==1'b1) begin
			current_top_state[7:0] <= CS2STS_next_top_state[7:0];
		end
	end
end




endmodule